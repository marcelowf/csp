#include "../include/cg.h"
#include "../include/bprice.h"

using namespace std;

int main(const int argc, char** argv) {
 if (argc != 7) {
	cout << "Error! Choose the correct params" << endl;
	//~ cout << "<Model> <Intance prefix> <initial number> <final number> <timelimit>" << endl;
	//~ cout << "Model: 1 = MILP relaxation; 2 = MILP,  3 = Column Generation; 4 = Branch and Price" << endl;
 }
 
 int modelId = atoi(argv[1]); 
 double timeLimit = stod(argv[2]);
 stringstream file;
 file << argv[3];
 int lfa = atoi(argv[4]);
 int minIt = atoi(argv[5]);
 int noImpPerc = atoi(argv[6]);
 
 
 switch (modelId){
	case 3: csp::cg(timeLimit, file.str(), lfa, minIt, noImpPerc);
	break;	
	default: cout << "Error, no Model with index " << modelId << " was not found! Try again!"<< endl;
	break;
 }
 
 
return 0;
}
