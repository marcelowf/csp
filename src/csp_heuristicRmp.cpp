#include <queue>          
#include <vector>
#include <limits>
#include <math.h>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"

#define RC_LB -1.0e-6
#define RC_UB 1.0e-5
#define NEAR_ONE 0.99999
#define M 1.0e+8

using namespace std;
using namespace csp;

double CGModel::heuristicRMP(const Instance& inst, IloEnv& env, const int& nodes){
	struct Column{
		int j;
		int col;		
		double relaxValue; //is the value to sort the priority queue
	};
	struct CompareColumn{
		public:
		bool operator()(const Column& c1,const Column& c2){
			return (c2.relaxValue > c1.relaxValue); //"<" for crescent order; ">" for decrescent
		}
	};
	
	int j, c, i;
	double varValue;	
	IloArray<IloArray<IloIntArray>> alpha(env, inst.wells+inst.compressors); //Set of columns that cover a point i [i][j][c1] where c1 is the index of column c in cjS, delta, etc, and i is a well or compressor 
	//BETA = DELTA Set of wells covered by each colum [j][c][i1] where i1 is the index of well that is supplied
	//initialize alpha TODO - initialize only once - set of columns that cover well i 
	//~ if(nodes == 1){
		for (i=0;i<inst.wells+inst.compressors;i++){
			alpha[i] = IloArray<IloIntArray>(env, inst.compressors);
			for (j=0;j<inst.compressors;j++){
				alpha[i][j] = IloIntArray(env);				
			}
			if (i < inst.wells){//Get the columns that supply well i 
				for (IloExpr::LinearIterator it = ClientServiced[i].getLinearIterator(); it.ok(); ++it){
					int type, j1;
					getColumnIndices(it.getVar(), j1, c, type);					
					if (delta[j1][c][i] >= NEAR_ONE){				
						alpha[i][j1].add(c);							
					}
					//~ alpha[j1+inst.wells][j1].add(c); //add the column to the point that corresponds to compressor
				}
			}else{//When considering the point of compressors, create an array of same size of delta[i-inst.wells], but with all values equal to 0;
				//~ alpha[i][i-inst.wells] = IloIntArray(env, delta[i-inst.wells].getSize());
				alpha[i][i-inst.wells].add(-1); //Add the pseudocolum -1 that representss just the compressor i-inst.wells
			}
		}	
	//~ }

	//Add columns to priority queue
	priority_queue<Column,vector<Column>, CompareColumn> pQC;	
	for (j = 0; j < inst.compressors; j++){		
		for (c = 0; c < relaxModelValues[j].getSize(); c ++){
			varValue = relaxModelValues[j][c]; //Variable value
			//~ varValue = cjS[j][c]/IloSum(delta[j][c]); //Coeficent divided by the number of supplied clients
			Column col{	j, c, varValue}; 
			pQC.push(col);
		}
	}	
///From here - Considering a set partitioning, where a column have size = m+n
	IloIntArray coveredPoint(env, inst.wells+inst.compressors); // Represents the number of columns that cover each point (the first m numbers are the wells and after the n numbers are the compressors)
	int numCoveredPoints = 0;
	int numCoveredWells = 0;
	double obj = 0;
	//Storage the columns in the solution vector<pair> s, vector t is for using after
	vector< pair<bool, Column> > s;
	vector<int> t;
	//Cosntruct a s based on the relaxation - Add columns until all WELLS are covered.  
	while(numCoveredWells < inst.wells){
		Column col = pQC.top();
		pQC.pop();		
		s.push_back(make_pair(true,col));
		t.push_back(t.size());
		for (i=0;i<inst.wells;i++){
			if(delta[col.j][col.col][i] > NEAR_ONE){
				coveredPoint[i]++;
				if(coveredPoint[i] == 1){ //When the point(well) are not covered yet
					numCoveredPoints++;
					numCoveredWells++;
				}
			}
		}
		coveredPoint[inst.wells+col.j]++; //point of compressor
		if(coveredPoint[inst.wells+col.j] == 1) //When the ponit(compressor) are not covered yet
			numCoveredPoints++;
		obj += cjS[col.j][col.col];
	}	
	//If some compressor is not covered yet
	if (numCoveredPoints < inst.wells+inst.compressors){
		for (j=inst.wells;j<inst.wells+inst.compressors;j++){
			if (coveredPoint[j] < 1){ //Find a compressor not covered
				coveredPoint[j]++;
				numCoveredPoints++;
				Column col1{j-inst.wells,-1, 0}; //When we use a set with just a compressor, the index of column is '-1' and its cost is zero
				s.push_back(make_pair(true,col1));
				t.push_back(t.size());
			}
		}		
	}
	
	//Remove columns that cause overcovered points
	while(t.size()>=1){
		//Randonmly select a index of column in t and remove it
		int r = iRand(0,t.size(),nodes*t.size());
		int x = t[r];
		t.erase(t.begin()+r);		
		//for each i in selected column verify if coveredPoint[i] (and j) is oversupplied
		bool remove = false;
		for(i=0; i<inst.wells+inst.compressors; i++){
			if (i==0 && s[x].second.col == -1) i = inst.wells; //If a column with just a compressor is selected, jump the index
			if (i<inst.wells){
				if (delta[s[x].second.j][s[x].second.col][i] >= NEAR_ONE && coveredPoint[i] > 1){
					remove = true;
					break;
				}
			}else{ //analizing the points related to the compressors
				if(coveredPoint[s[x].second.j+inst.wells] > 1){ //if just a compressor is oversupplied
					remove = true;
					break;
				}
			}
		}
		
		//Remove the column in the solution if it cause overcover
		if (remove){
			//used when a 'real column' is selected
			if(s[x].second.col != -1){
				obj -= cjS[s[x].second.j][s[x].second.col];
				s[x].first = false;
				for (i=0;i<inst.wells;i++){
					if(delta[s[x].second.j][s[x].second.col][i] >= NEAR_ONE){
						coveredPoint[i]--;
						if (coveredPoint[i] == 0) numCoveredPoints--;
					}
				}
			}
			//Remove the compressor
			coveredPoint[inst.wells+s[x].second.j]--;
			if(coveredPoint[inst.wells+s[x].second.j] == 0) numCoveredPoints--;		
		}			
	}
	
	///Re-insert columns for the uncovered points such that does not cause overcover
	//Storage the unsuplied points
	vector <int> v;//auxiliar vector of uncovered points
	for (i=0; i<inst.wells+inst.compressors; i++){
		if (coveredPoint[i] == 0){
			v.push_back(i);
		} 		
	}
	//Select randomly an unsuplied point from v - (r is the index in vector v and x is the index in coveredPoint)
	selectRand:	
	while(v.size()>=1){		
		int r = iRand(0,v.size(),nodes*v.size());		
		int x = v[r];				
		//Verify if x is already covered - GAMBIARRA
		if (coveredPoint[x] == 1){
			v.erase(v.begin()+r);
			goto selectRand;
		}
		v.erase(v.begin()+r); //Erase the point
		int totalCover=0, minVal = M, jMin=-1, cMin=-1;
		bool candidate = true;
		if (x < inst.wells){ //If x is a well			
			//Search for a column that cover x and minimize Csj/|Beta_j|
			for (j=0;j<alpha[x].getSize();j++){ //Para cada compressor que pode atender x				
				if(!coveredPoint[inst.wells+j] == 1){ //Cannot search for a column which is of a already covered compressor
					for(c=0;c<alpha[x][j].getSize();c++){ //Para cada coluna do compressor j que pode atender x
						totalCover++; //refers to the compressor
						//Verify if column supply another points or an already covered point
						for (i=0;i<inst.wells;i++){
							if(delta[j][c][i] >= NEAR_ONE){
								if (coveredPoint[i] == 1){//the column cannot be add, go to the next									
									candidate = false;
									break;
								}else{
									totalCover++;
								}
							}
						}
						if(candidate){ //If the column is a candidate to be add
							double val = cjS[j][c]/totalCover;
							if (val < minVal){
								minVal = val;
								jMin=j;
								cMin=c;
							}
						}
						totalCover = 0;						
					}
				}
			}
		}else{ //x is a compressor. Find in delta, including the column that contain only x
			//Find first for a "real" column			
			for(c=0;c<delta[x-inst.wells].getSize();c++){ //For each column of compressor 'x'
				for(i=0;i<inst.wells;i++){ //For each well in the column
					if(delta[x-inst.wells][c][i] >= NEAR_ONE){
						if(coveredPoint[i] == 1){
							candidate = false;
							break;
						}else{
							totalCover++;
						}
					}
				}
				if(candidate){
					double val = cjS[x-inst.wells][c]/totalCover;
					if (val < minVal){
						minVal = val;
						jMin=x-inst.wells;
						cMin=c;
					}
				}
				totalCover = 0;
			}
			//if no column was found for a compressor, consider the column that cover only the compressor
			jMin=x-inst.wells;
		}
		minVal = M;
		//If a column was found
		if(jMin != -1){
			//Cover the correspondendt compressor
			coveredPoint[inst.wells+jMin]++;
			numCoveredPoints++;
			//Cover the wells if it is a 'real column'
			if(cMin != -1){
				for (i=0;i<inst.wells;i++){
					if (delta[jMin][cMin][i] > NEAR_ONE){
						if (coveredPoint[i] > 0) cout << "ERRO" << endl;
						coveredPoint[i]++;
						numCoveredPoints++;						
					}
				}
				obj += cjS[jMin][cMin];
			}
		}
	}
	//~ cout << coveredPoint << endl;	
	if (numCoveredPoints < inst.wells+inst.compressors) return -1;
	return obj;
}
