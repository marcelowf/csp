#include "../include/cg.h"
#include "../include/bprice.h"

using namespace std;

int main(const int argc, char** argv) {
 if (argc != 7) {
	cout << "Error! Choose the correct params" << endl;
	cout << "<Model> <Intance prefix> <initial number> <final number> <timelimit> <coeficentRMP>" << endl;
	cout << "Model: 1 = MILP relaxation; 2 = MILP,  3 = Column Generation; 4 = Branch and Price" << endl;
 }
 
 int modelId = atoi(argv[1]);
 stringstream file;
 file << argv[2];
 int n_ini = atoi(argv[3]);
 int n_fim = atoi(argv[4]);
 double timeLimit = stod(argv[5]);
 double coefRmp = stod(argv[6]);
 switch (modelId){
	case 1: csp::milp(file.str(), n_ini, n_fim, 1, timeLimit);
	break;
	case 2: csp::milp(file.str(), n_ini, n_fim, 2, timeLimit);
	break;
	case 3: csp::cg(file.str(), n_ini, n_fim, timeLimit);
	break;
	case 4: csp::bPrice(file.str(), n_ini, n_fim, timeLimit, coefRmp);
	break;
	case 5: csp::milpSos2(file.str(), n_ini, n_fim, 5, timeLimit);
	break;
	case 6: csp::milpSos2(file.str(), n_ini, n_fim, 6, timeLimit);
	break;
	default: cout << "Error, no Model with index " << modelId << " was not found! Try again!"<< endl;
	break;
 }
 
 
return 0;
}
