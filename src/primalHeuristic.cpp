double CGModel::primalHeuristic(const Instance& inst, IloEnv& env){	
	struct Column{
		int j;
		int col;		
		double relaxValue;
	};
	struct CompareColumn{
		public:
		bool operator()(const Column& c1,const Column& c2){
			return (c2.relaxValue > c1.relaxValue);
		}
	};
	//To storage the wells that was nos supplied yet
	struct Well{
		int i;
		double demand;
	};
	struct CompareWell{
		public:
		bool operator()(const Well& w1,const Well& w2){
			return (w1.demand > w1.demand);
		}
	};	

	priority_queue<Well,vector<Well>, CompareWell> pQW;
	IloIntArray installCompressors(env); //Vetor com compressores instalados
	IloNumArray gasRate(env, inst.compressors); 
	IloNumArray operCosts(env, inst.compressors); 
	IloNumArray maxGasRate(env, inst.compressors); 
	IloIntArray solutionWell(env, inst.wells); // Indicate the index compressor that  supply the well
	IloIntArray active_compressors(env); //Storage the already active compressors
	double obj = 0.0;
	int unLocatedWells = inst.wells;

	//Storage the columns by their value of correspondent variable in the RMP
	priority_queue<Column,vector<Column>, CompareColumn> pQC;	
	for (int j = 0 ; j < inst.compressors; j++){				
		maxGasRate[j] = inst.qCMax[j];		
		for (int c = 0; c < lambda[j].getSize(); c++){
			if (cplex.getValue(lambda[j][c]) > RC_UB){
				Column col{	j, c, cplex.getValue(lambda[j][c])};			
				pQC.push(col);
			}
		}
	}
	
	for (int i = 0; i < inst.wells; i++){
		solutionWell[i] = -1;		
	}

///First phase - alocate coluns until all wells has been instaled
	 while(unLocatedWells > 0){	 
		if (pQC.size() > 0){
			Column col = pQC.top();	//Get the columns by decrease value of the variable			
			pQC.pop();
			for (int i = 0; i < inst.wells; i++){			 
				if (delta[col.j][col.col][i] >= NEAR_ONE){ // If the well is supplied in the column
					if (solutionWell[i] == -1){ //If the well isn't meet yet - allocate to the compressor
						if(gasRate[col.j] + inst.qW[i] <= inst.qMaxIJ[col.j][i] && gasRate[col.j] + inst.qW[i] <= maxGasRate[col.j]){ //If the compressor can suppy this well
							solutionWell[i] = col.j;
							gasRate[col.j] += inst.qW[i];
							obj += inst.cIJ[col.j][i];
							operCosts[col.j] = inst.dJ[col.j]*gasRate[col.j]*getPressure(inst, col.j, gasRate[col.j]);
							unLocatedWells--;
							if (inst.qMaxIJ[col.j][i] < maxGasRate[col.j]){
								maxGasRate[col.j] = inst.qMaxIJ[col.j][i];
							}							
						}						
					}
					else if (inst.cIJ[col.j][i] < inst.cIJ[solutionWell[i]][i] && //client already supplied - test if the cost is less, and the capacities 
					 gasRate[col.j] + inst.qW[i] <= inst.qMaxIJ[col.j][i] &&
					 gasRate[col.j] + inst.qW[i] <= maxGasRate[col.j]){
						int old_j = solutionWell[i];					
						solutionWell[i] = col.j;
						//Remove the values of the old compressor
						int active = 0;
						gasRate[old_j] -= inst.qW[i];					
						obj -= inst.cIJ[old_j][i];
						if (maxGasRate[old_j] == inst.qMaxIJ[old_j][i]){ //Se o menor valor de MaxGasRate era devido a esse poço que 'sai'
							for (int x = 0; x < inst.wells; x++){
								if (solutionWell[x] == old_j){
									active = 1;
									if (inst.qMaxIJ[old_j][x] < maxGasRate[old_j]){
										maxGasRate[old_j] = inst.qMaxIJ[old_j][x];
									}
								}
							}
							if (active == 0){		//If the old compressor doesn't supply any well now
								maxGasRate[old_j] = inst.qCMax[old_j];
								operCosts[old_j] = 0.0;
							}else{
								operCosts[old_j] = inst.dJ[old_j]*gasRate[old_j]*getPressure(inst, old_j, gasRate[old_j]);
							}
						}
						//Update the compressor with the new well 
						gasRate[col.j] += inst.qW[i];
						obj += inst.cIJ[col.j][i];
						operCosts[col.j] = inst.dJ[col.j]*gasRate[col.j]*getPressure(inst, col.j, gasRate[col.j]);
						if (inst.qMaxIJ[col.j][i] < maxGasRate[col.j]){
							maxGasRate[col.j] = inst.qMaxIJ[col.j][i];
						}
					}	
				}		 
			}
		}else{ //If wells remains unsupplied
			for (int i = 0; i < solutionWell.getSize(); i++){ //Storage the wells unsupplien in a PQ
				if (solutionWell[i] == -1){
					Well well{i, inst.qW[i]};
					pQW.push(well);
				}
			}			
			for (int j = 0; j < gasRate.getSize(); j++){
				if (gasRate[j] > RC_UB){
					active_compressors.add(j);					
				}
			}
			findCompressor:			
			while(pQW.size() > 0){
				Well well = pQW.top();
				pQW.pop();
				for (int j = 0; j < active_compressors.getSize(); j++){
					int act_comp = active_compressors[j];
					if (inst.orac[act_comp][well.i] == 1){
						//If an active compressor can supply the well.i
						if (gasRate[act_comp] + inst.qW[well.i] <= inst.qCMax[act_comp] &&
						gasRate[act_comp] + inst.qW[well.i] <= maxGasRate[act_comp]){
							unLocatedWells--;
							solutionWell[well.i] = act_comp;
							obj += inst.cIJ[act_comp][well.i];
							gasRate[act_comp] += inst.qW[well.i];
							operCosts[act_comp] = inst.dJ[act_comp]*gasRate[act_comp]*getPressure(inst, act_comp, gasRate[act_comp]);
							if (inst.qMaxIJ[act_comp][well.i] < maxGasRate[act_comp]){
								maxGasRate[act_comp] = inst.qMaxIJ[act_comp][well.i];
							}
							goto findCompressor;
						}
					}
				}
				// If no active compressor can suppli well.i
				for (int j = 0; j < inst.compressors; j++){
					//if the compressor is not active yet
					if (gasRate[j] <= RC_UB){ 
						if (inst.orac[j][well.i] == 1){
							solutionWell[well.i] = j;
							obj += inst.cIJ[j][well.i];
							gasRate[j] += inst.qW[well.i];
							operCosts[j] = inst.dJ[j]*gasRate[j]*getPressure(inst, j, gasRate[j]);
							maxGasRate[j] = inst.qMaxIJ[j][well.i];
							active_compressors.add(j);
							unLocatedWells--;							
							break;							
						}
					}
				}
			}
		}
	}
	obj += IloSum(operCosts);
	if (active_compressors.getSize() == 0){
		for (int j = 0; j < inst.compressors; j++){
			if (gasRate[j] > RC_UB){
				obj+= inst.cJ[j];
				active_compressors.add(j);
			}			
		}
	}else{
		for (int j = 0; j < active_compressors.getSize(); j++){
			obj += inst.cJ[active_compressors[j]];
		}
	}
	cout << "Obj before local search " << obj << endl;
///Second phase local search
	//Shift
	//~ int improvment;
	//~ do{
		//~ improvment = 0;
		//~ int best_j = -1;			
		//~ int best_i = -1;			
		//~ double best_profit = 0;		
		//~ for (int i = 0; i < inst.wells; i++){
			//~ for (int j = 0; j < active_compressors.getSize(); j++){
				//~ int act_comp = active_compressors[j];
				//~ if(solutionWell[i] != act_comp){ //Other compressor
					//~ if (inst.orac[act_comp][i] == 1){ //Can supply the well
						//~ if(gasRate[act_comp] + inst.qW[i] <= inst.qCMax[act_comp] &&
							//~ gasRate[act_comp] + inst.qW[i] <= maxGasRate[act_comp]){ //And the compressor has capacity to this new well
							//~ double profit = shiftProfit(inst, i, solutionWell[i], act_comp, operCosts[solutionWell[i]], operCosts[act_comp], gasRate[solutionWell[i]], gasRate[act_comp]);
							//~ if(profit < 0){ //Reduces the objective 
								//~ if (profit < best_profit){
									//~ best_profit = profit;
									//~ best_j = act_comp;
									//~ best_i = i;
									//~ improvment = 1;
								//~ }
							//~ }
						//~ }
					//~ }
				//~ }
			//~ }
		//~ }
		//~ if (best_j >= 0){
			//~ //Remove the well from the old compressor
			//~ gasRate[solutionWell[best_i]] -= inst.qW[best_i];				
			//~ operCosts[solutionWell[best_i]] = inst.dJ[solutionWell[best_i]]*gasRate[solutionWell[best_i]]*getPressure(inst, solutionWell[best_i], solutionWell[best_i]);
			//~ if (gasRate[solutionWell[best_i]] <= RC_UB){
				//~ obj -= inst.cJ[solutionWell[best_i]];
				//~ for (int j = 0; j < active_compressors.getSize();  j++){
					//~ if (active_compressors[j] == solutionWell[best_i]){						
						//~ active_compressors.remove(j);
						//~ break;
					//~ }
				//~ }
			//~ }			
			//~ //Shift the well
			//~ solutionWell[best_i] = best_j;
			//~ gasRate[best_j] += inst.qW[best_i];
			//~ if (inst.qMaxIJ[best_j][best_i] < maxGasRate[best_j]){
				//~ maxGasRate[best_j] = inst.qMaxIJ[best_j][best_i];
			//~ }
			//~ operCosts[best_j] = inst.dJ[best_j]*gasRate[best_j]*getPressure(inst, best_j, gasRate[best_j]);
			//~ //update the obj with a negative value
			//~ obj += best_profit;			
		//~ }
		//~ 
	//~ }while(improvment > 0);

	//Recalculate obj to check the correct value	
	#ifndef NDEBUG
	double realObj = 0;
	for (int j = 0; j < inst.compressors; j++){
		double gRate = 0;
		if(gasRate[j] > RC_UB){
			realObj += inst.cJ[j];
			for (int i = 0; i < inst.wells; i++){
				if (solutionWell[i] == j){
					realObj += inst.cIJ[j][i];
					gRate += inst.qW[i];
				}
			}
		realObj += inst.dJ[j]*gRate*getPressure(inst, j, gRate);
		}		
	}
	cout << "Obj " << obj << " Real obj " << realObj << endl;
	//~ assert(obj == realObj);
	#endif
	
	return obj;
	//~ return realObj;
}

///Primeira estratégia gulosa - Procura pela fracionária de maior valor e integraliza ela e resolve o master de novo, repete até chegar a uma solução inteira
double CGModel::primalHeuristic(const Instance& inst, IloEnv& env){	
	IloRangeArray gHeuristic(env, inst.compressors, 1 , 1);		
	int status=1;
	double obj;
	do{
		int j, c, comp, col;
		double maxValue = RC_UB, varValue;
		for (j = 0; j < inst.compressors; j++){
			for (c = 0; c < lambda[j].getSize(); c++){					
				varValue = cplex.getValue(lambda[j][c]);			
				if (varValue > RC_UB && varValue <= NEAR_ONE){
					if (varValue > maxValue){
						maxValue  = varValue;
						comp = j;
						col = c;
					}
				}
			}
		}
		gHeuristic[comp].setExpr(lambda[comp][col]);		
		model.add(gHeuristic[comp]);
		if (!cplex.solve()) goto giveUp;		
		obj = cplex.getObjValue();
		status = getStatus(inst.compressors);
	}while (status != 2);

	model.remove(gHeuristic);
	return obj;

	giveUp:	
	model.remove(gHeuristic);
	return {numeric_limits<double>::max()};		
}
