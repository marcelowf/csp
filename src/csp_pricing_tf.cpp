#include <vector>
#include <limits>
#include <math.h>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"

#define RC_LB -1.0e-6
#define RC_UB 1.0e-5
#define NEAR_ONE 0.99999
#define M 1.0e+8
//~ #define RANDOMSOLUTIONA
#define RANDOMCANDIDATE

using namespace std;
using namespace csp;

ILOSTLBEGIN

Solution CGModel::getReducedCost(IloEnv& env, const Instance& inst, const int& lfa, const int& j, 
const IloNum& mi, const IloNumArray& pi, const IloNumArray& psy, const IloIntArray& seeds, const int& cgIt, const int& nodes, const IloArray<csp::pricing>& subproblem,
const int& minIt, const int& noImpPerc){
	//Auxiliar
	int m = inst.wells;
	int n = inst.compressors;
	int i,k;
	
	//Build a initial (and current) random solution 
	Solution s = randomS(env, inst, j, mi, pi, psy, seeds[0]+j+cgIt+nodes, subproblem);	
	//Build the list of objective funcion values (reduced costs)
	vector<double> F(lfa,s.rc);	
	Solution sBest = s;
	//Loop	
	int it = 1;	
	int noImprov = 0;
	do{
		#ifdef RANDOMCANDIDATE
		Solution sC = randomS(env, inst, j, mi, pi, psy, seeds[it]+j+cgIt+nodes, subproblem);			
		#endif
		#ifndef RANDOMCANDIDATE		
		Solution sC;
		sC = copySolution(env, s);		
		//select a random well and flip the bit
		int w = iRand(0,inst.wells,seeds[it]+j+cgIt);			
		double operCosts = inst.dJ[j]*getPressure(inst,j,sC.q)*sC.q;
		sC.rc -= operCosts;
		sC.c -= operCosts;		
		if (sC.x[w] == 1){
			sC.x[w] = 0;			
			sC.q -= inst.qW[w];
			sC.c -= inst.cIJ[j][w];
			sC.rc -= (inst.cIJ[j][w]-pi[w]-psy[w]);
			operCosts =   inst.dJ[j]*getPressure(inst,j,sC.q)*sC.q; //update operating cost
			sC.c += operCosts;
			sC.rc += operCosts;
			//Update qMin			
			if(sC.qMin == inst.qMaxIJ[j][w]){
				double qMin = inst.qCMax[j];
				for (i=0;i<sC.x.getSize();i++){
					if (sC.x[i] == 1){
						if (inst.qMaxIJ[j][i] < qMin) qMin = inst.qMaxIJ[j][i];
					}
				}
				sC.qMin = qMin;
			}						
		}else{//add a well
			if (inst.orac[j][w] == 1 && sC.q+inst.qW[w] <= inst.qMaxIJ[j][w] && sC.q+inst.qW[w] <= sC.qMin){
				if (inst.qMaxIJ[j][w] < sC.qMin) sC.qMin = inst.qMaxIJ[j][w];
				sC.x[w] = 1;
				sC.q += inst.qW[w];
				sC.c += inst.cIJ[j][w];
				sC.rc += (inst.cIJ[j][w]-pi[w]-psy[w]);
				operCosts = inst.dJ[j]*getPressure(inst,j,sC.q)*sC.q; //update operating cost
				sC.c += operCosts;
				sC.rc += operCosts;
			}else{ //No aceptable moving - create a random solution
				sC = randomS(env, inst, j, mi, pi, psy, seeds[it]+j+cgIt, subproblem);	
				//~ cout << sC.x << " - " << s.x << endl;				
			}
		}		
		#endif		
		int v = it % lfa;
		bool improved = false;
		if (sC.rc <= F[v] || sC.rc <= s.rc){						
			F[v] = sC.rc;
			s = sC;//copySolution(env, sC);
			if (sC.rc < sBest.rc && fabs(sBest.rc - sC.rc) > RC_UB){				
				sBest = sC;//copySolution(env, sC);			
				noImprov = 0;
				improved = true;
			}
		}
		if (!improved)  noImprov++;
		it++;			
	}while(noImprov < ceil(it*noImpPerc/100) || it < minIt); 
	
	return sBest;
}

Solution CGModel::copySolution(IloEnv& env, const Solution& s){
	Solution copy;
	copy.x = IloNumArray(env, s.x.getSize());
	for (int i=0;i<copy.x.getSize();i++) copy.x[i] = s.x[i];
	copy.q = s.q;
	copy.c = s.c;
	copy.rc = s.rc;
	copy.qMin = s.qMin;
	
	return copy;
}

Solution CGModel::randomS(IloEnv& env, const Instance& inst, const int& j, const IloNum& mi, const IloNumArray& pi, const IloNumArray& psy, const int& seed, const IloArray<csp::pricing>& subproblem){
	Solution s;
	s.x = IloNumArray(env,inst.wells);
	s.q = 0;		
	s.rc = inst.cJ[j]-mi;
	s.c = inst.cJ[j];
	s.qMin = inst.qCMax[j];
	
	#ifdef RANDOMSOLUTIONA //Select or not a well in the well sequency
	for (int i=0;i<inst.wells;i++){
		if (inst.orac[j][i]==1){
			int rand = iRand(0,2,seed+i);			
			if(rand == 1 && s.q+inst.qW[i] <= inst.qMaxIJ[j][i] && s.q+inst.qW[i] <= s.qMin){
				//Update solution only if compressor has capacity
				s.x[i] = 1;
				s.q += inst.qW[i];
				s.rc += inst.cIJ[j][i]-pi[i]-psy[i];
				s.c += inst.cIJ[j][i];
				if (inst.qMaxIJ[j][i] < s.qMin) s.qMin = inst.qMaxIJ[j][i];				
			}
		}
	}
	#endif
	#ifndef RANDOMSOLUTIONA //select a random well
	//Verify the branch rules
	for (int i=0; i<inst.wells; i++){
		if (subproblem[j].x[i].getLB() == 1){ //If well i must be supplied in the branch rule
			s.x[i] = 1;
			s.q += inst.qW[i];
			s.rc += (inst.cIJ[j][i]-pi[i]-psy[i]);
			s.c += inst.cIJ[j][i];
			if (inst.qMaxIJ[j][i] <s.qMin) s.qMin = inst.qMaxIJ[j][i];
		}
	}
	for (int i=0; i<inst.wells; i++){
		int w = iRand(0,inst.wells,seed+i);
		if(subproblem[j].x[w].getUB() == 1 && inst.orac[j][w] >= NEAR_ONE && s.q+inst.qW[w] <= inst.qMaxIJ[j][w] && s.q+inst.qW[w] <= s.qMin && s.x[w] != 1){
			s.x[w] = 1;
			s.q += inst.qW[w];
			s.rc += (inst.cIJ[j][w]-pi[w]-psy[w]);
			s.c += inst.cIJ[j][w];
			if (inst.qMaxIJ[j][w] <s.qMin) s.qMin = inst.qMaxIJ[j][w];
		}
	}
	#endif
	//assert(s.q <= inst.qCMax[j]);
	double p = getPressure(inst, j, s.q);
	s.c += inst.dJ[j]*s.q*p;
	s.rc += inst.dJ[j]*s.q*p;
	return s;	
}
