// -------------------------------------------------------------- -*- C++ -*-
// Autor: Marcelo Friske
// Date: November/2015
// Description: MILP Formulation for Compressor Scheduling Problem, picewise linear formulation
// with SOS2 variables for piecewise-linearization. Based on: "A revised model for compressor design and scheduling
// in gas-lifted oil fields". E. Camponogara, L. Nazari and C. Meneses (2011).
// 
// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"

#define NEAR_ONE 0.999999
#define RC_UB 1.0e-6

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using csp::Instance;

double getPress(const Instance& inst, const int& j, const double& qj){
	double gasRate;
	if (qj <= 0){
		gasRate = inst.qCMin[j];		
	}else {
		gasRate = qj;
	}
	return inst.alpha0[j] + inst.alpha1[j]*gasRate + inst.alpha2[j]*(pow(gasRate,2.0)) + 
	inst.alpha3[j]*(pow(gasRate,3.0)) + inst.alpha4[j]*(log(1 + gasRate));	
}
	
void csp::milpSos2(string file, const int n_ini, const int n_fim, const int type, const double& timeLimit){	
	stringstream out;
	out << type << "_Inst_" << n_ini << "_to_" << n_fim << "_" << timeLimit << ".txt";
	ofstream output(out.str());
	output << fixed;
	output.precision(3);	
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	output << "I\t" << "N\t" << "M\t" << "Cplex\t" << "Total\t" << "Obj\t" << "Nodes\t c_A\t c_B\t c_C\t c_D\t c_E\t c_F\t c_G\t c_H\t c_I\t c_J\t c_K\t c_L\t c_M\t c_N\t c_O\t c_P\t c_Q\t" << "Solution" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		Timer<chrono::milliseconds> timer_global(timeLimit * 1000);
		timer_global.start();
		
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		float cplex_time {0};
	
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
		
	   try {
		  IloModel model(env); 
		  int i, j;
		  int n = inst.compressors;
		  int m = inst.wells;
		  int k = inst.k;
		  
		  /**** Variables ****/
		  IloNumVarArray y;
		  if (type == 5)  y = IloNumVarArray(env, n, 0, 1, ILOINT); // yi -> facilities {0,1}
		  else  y = IloNumVarArray(env, n, 0, 1, ILOFLOAT); 
		  y.setNames("y");
		  IloArray<IloNumVarArray> x(env, n); 				  // xij -> facility i assign to cliente j {0,1}
		  for (i = 0; i < n; i++){
			if (type == 5) x[i] = IloNumVarArray (env, m, 0, 1, ILOINT);			
			else x[i] = IloNumVarArray (env, m, 0, 1, ILOFLOAT);
			x[i].setNames("x");
		}		  
		  		  
		  IloNumVarArray q(env, n, 0, IloInfinity, ILOFLOAT); // qj -> gas-rate facility j
		  q.setNames("q");
		  IloSOS2Array lambda(env, n);
		  NumVarMatrix lambdaL(env, n);
		  			
		  for (int j = 0; j < n; j++){
			lambdaL[j] = IloNumVarArray (env, k, 0.0, 1.0, ILOFLOAT);			
			lambda[j] = IloSOS2(env, lambdaL[j]);			
		  }
		  /***/		  
		  IloExpr expr(env); 
		  expr.clear();
		  /** Objective function ***/
		  
		  for(j = 0; j < n; j++){
			  expr += inst.cJ[j]*y[j]; 
		  }
		  for (j = 0; j < n; j++){
			for (i = 0; i < m; i++){
				if(inst.orac[j][i] == 1) expr += inst.cIJ[j][i]*x[j][i]; 
			}
		  }
		  for (j = 0; j < n; j++){
			  for (i = 0; i < k; i++){
				expr += (inst.pointsH[j][i]*lambdaL[j][i]);
			  }         
		  }
		  model.add(IloMinimize(env, expr));
		  
		  /***Constraints***/
		  //1b - Sinlge source
		  for (i = 0; i < m; i++){
			for (j = 0; j < n; j++){
				if(inst.orac[j][i] == 1) model.add(x[j][i] <= y[j]);
			}		
		  }
		  //1c - Supply demand
		  for(i = 0; i < m; i++){
			expr.clear();
			for (j = 0; j < n ; j++){
				if(inst.orac[j][i] == 1) expr += x[j][i];
			}
			model.add(expr == 1); 
		  }	
		  //1d - Max capacity
		  for(j = 0; j < n; j++){
			for(i = 0; i < m; i++){
				if(inst.orac[j][i] == 1)	model.add(q[j] <= inst.qMaxIJ[j][i]*x[j][i] + inst.qCMax[j]*(y[j] - x[j][i]));
			}
		  }
		  //6c - Piecewise gas rate
		  IloExpr exp1(env);
		  for(j = 0; j < n; j++){			
			expr.clear();
			exp1.clear();
			for(i = 0; i < k; i++){
				expr += (inst.pointsQ[j][i]*lambdaL[j][i]);
				exp1 += lambdaL[j][i];
			}  
			model.add(q[j] == expr);
			model.add(exp1 == y[j]);			
		  }
		  model.add(lambda);
		  //1f
		  for(int j = 0; j < n; j++){
			  expr.clear();
			  for (i = 0; i < m; i++){
				if(inst.orac[j][i] == 1)	expr += inst.qW[i]*x[j][i];
			  }
			 model.add(expr <= q[j]); 
			 //~ model.add(expr == q[j]); //Original do Artigo
		  }
	
		IloCplex cplex(model);

		//~ cplex.setParam(IloCplex::PreInd, false);
		cplex.setParam(IloCplex::Threads, 1);
		//~ cplex.setParam(IloCplex::Covers, -1);
		//~ cplex.setParam(IloCplex::GUBCovers, -1);
		//~ cplex.setParam(IloCplex::FlowCovers, -1);
		//~ cplex.setParam(IloCplex::Cliques, -1);
		//~ cplex.setParam(IloCplex::FracCuts, -1);
		//~ cplex.setParam(IloCplex::DisjCuts, -1);		
		//~ cplex.setParam(IloCplex::FlowPaths, -1);		
		//~ cplex.setParam(IloCplex::ImplBd, -1);
		//~ cplex.setParam(IloCplex::MIRCuts, -1);
		//~ cplex.setParam(IloCplex::ZeroHalfCuts, -1);
		
		cplex.setOut(env.getNullStream());
		cplex.setParam(IloCplex::ClockType, 2);
		cplex.setParam(IloCplex::TiLim, timeLimit);
		timer_ms.start();
		cplex.solve();
		cplex_time += timer_ms.total();		
		//~ cout << "Best solution in " << timeLimit << " seconds: " << cplex.getObjValue() << endl;

		NumNumMatrix piecewise (env, n);
		for (int j = 0; j < n; j++){
			piecewise[j] = IloNumArray(env, k);
			cplex.getValues(lambdaL[j], piecewise[j]);
			cout << piecewise[j] << endl;
		} 
		
		float totalTime = timer_global.total();
		//~ output << inst.compressors << "-" << inst.wells << endl;
		//~ output << "Total Time: " << totalTime << endl;
		//~ output << "Time spend to MILP: " << cplex_time << " (" << cplex_time/totalTime*100 << "%)" << endl;		
		//~ output << "FO: " << cplex.getObjValue() << endl;
		if (cplex.getStatus() == IloAlgorithm::Unknown){
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << cplex_time/1000 << "\t" << totalTime/1000 << "\t XXX" << "\t";
		}else{
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << cplex_time/1000 << "\t" << totalTime/1000 << "\t" << cplex.getObjValue() << "\t";  
		}

		output << cplex.getNnodes() << "\t" << cplex.getNcuts(IloCplex::CutCover) << "\t"
		<< cplex.getNcuts(IloCplex::CutGubCover) << "\t"
		<< cplex.getNcuts(IloCplex::CutFlowCover) << "\t"
		<< cplex.getNcuts(IloCplex::CutClique) << "\t"
		<< cplex.getNcuts(IloCplex::CutFrac) << "\t"
		<< cplex.getNcuts(IloCplex::CutMCF) << "\t"
		<< cplex.getNcuts(IloCplex::CutMir) << "\t"
		<< cplex.getNcuts(IloCplex::CutFlowPath) << "\t"
		<< cplex.getNcuts(IloCplex::CutDisj) << "\t" 
		<< cplex.getNcuts(IloCplex::CutImplBd) << "\t" 
		<< cplex.getNcuts(IloCplex::CutZeroHalf) << "\t"
		<< cplex.getNcuts(IloCplex::CutLocalCover) << "\t"
		<< cplex.getNcuts(IloCplex::CutTighten) << "\t" 
		<< cplex.getNcuts(IloCplex::CutObjDisj) << "\t" 
		//~ << cplex.getNcuts(IloCplex::CutLiftProj) << "\t"
		<< cplex.getNcuts(IloCplex::CutUser) << "\t" 
		<< cplex.getNcuts(IloCplex::CutTable) << "\t" 
		<< cplex.getNcuts(IloCplex::CutSolnPool) ;
		output << endl;
		
		
		//~ IloNumArray vals(env);
		//~ cplex.getValues(vals, y);
		//~ double final_cost = 0;
		//~ IloNumArray gasR(env, inst.compressors);
		//~ for (int i = 0; i < n; i++){
			//~ if (vals[i] > NEAR_ONE){
				//~ final_cost += inst.cJ[i];
			//~ }
			//~ output << i << "{";
			//~ for (int j = 0; j < m; j++){
				//~ if (inst.orac[i][j] == 1){
					//~ if (cplex.getValue(x[i][j]) >= NEAR_ONE){
						//~ output << j << ",";
						//~ gasR[i] += inst.qW[j];
						//~ final_cost += inst.cIJ[i][j];
					//~ }
					//~ 
				//~ }
			//~ }
			//~ output << "} ";
		//~ }
		//~ for (int j = 0; j < inst.compressors; j++){
			//~ if (gasR[j] > RC_UB){
				//~ final_cost += inst.dJ[j] * gasR[j] * getPress(inst, j, gasR[j]);
			//~ }
		//~ }
		//~ output << "Final cost (aprox.) " << final_cost << endl;
	
	   }
	   catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
	   }
	   catch (...) {
		  cerr << "Unknown exception caught" << endl;
	   }
		env.end();	   	
		
		
   }
   output << "Legend: " << endl;
   output << "c_A = CutCover \n" << "c_B = CutGubCover \n" << "c_C = CutFlowCover \n" << "c_D = CutClique \n" << "c_E = CutFraq \n" << 
   "c_F = CutMCF \n" << "c_G = CutMir \n" << "c_H = CutFlowPath \n" << "c_I = CutDisj \n" << "c_J = CutImplBd \n" << 
   "c_K = CutZeroHalf \n" << "c_L = CutLocalCover \n" << "c_M = CutTighten \n" << "c_N = CutObjDisj \n" << "c_O = CutUser \n" << 
   "c_P = CutTable \n" << "c_Q = CutSolnPool \n";
}
