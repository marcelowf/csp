// Date: March/2015
// Description: Column Generation for Compressor Scheduling Problem
// Based on: "Column Generation for Solvin a Compressor Scheduling Problem" and "A revised Model for compressor design and scheduling in gas-lifted oil fields
// Eduardo Camponogara and Augustinho Plucenio
// To use value of compressor operation costs in CG, use the calculation of effective reduced cost, otherwise, commnet the code in the verificiation
// 'if efective_rc < 0'

// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"
#define NDEBUG
#define NCOUT
#define NEarlyBranch
//~ #define NCuts
//~ #define NInitialColumns
//~ #define NUSES0S2
#define NUSEBINARY
#define NADDMIPSTARTS 
#define TWOPHASE
#include <assert.h>

#define RC_LB -1.0e-6
#define RC_UB 1.0e-5
#define NEAR_ONE 0.9999
#define DELIMITER "_"
#define M 1.0e+6
#define DecPrecision 1.0e+6

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;
typedef std::numeric_limits< double > dbl;

using namespace std;
using csp::Instance;
using csp::CGModel;
using csp::pricing;

double CGModel::getPressure(const Instance& inst, const int& j, const double& qj){
	return inst.alpha0[j] + inst.alpha1[j]*qj + inst.alpha2[j]*(pow(qj,2.0)) + 
	inst.alpha3[j]*(pow(qj,3.0)) + inst.alpha4[j]*(log(1 + qj));	
}

double CGModel::costColum(const IloInt& wells, const IloNumArray& delta, const IloInt& compressor, const IloNumArray& cJ, 
				 const NumNumMatrix& cIJ, const IloNumArray& dJ, const IloNum& qC, const IloNum& pC){
	  IloNum cost = cJ[compressor];	  
	  cost += IloScalProd(cIJ[compressor], delta);	
	  cost += dJ[compressor]*qC*pC;  
	  //~ cost = floor(cost * DecPrecision) / DecPrecision;
	  return cost;
}
  		
void CGModel::init(IloEnv& env, const int& compressors, const int& wells){
    env.setNormalizer(IloFalse);
    cplex.setOut(env.getNullStream()); //NÃ£o mostrar iteraÃ§Ãµes na tela.
	//~ cplex.setDeleteMode(IloCplex::FixBasis);
	//~ cplex.setParam(IloCplex::RootAlg, IloCplex::Dual);
	//~ cplex.setParam(IloCplex::NumericalEmphasis, 1);    //PrecisÃ£o numÃ©rica	
	//~ cplex.setParam(IloCplex::AdvInd, 2); // CalibraÃ§Ã£o inicio de MIPStarts; Advanced Information
	cplex.setParam(IloCplex::Threads, 1);
	cplex.setParam(IloCplex::ClockType, 2);
	
    lambda = NumVarMatrix(env,compressors); //Array de Array de variÃ¡veis [n]x[#col]      
    delta = IloArray<NumNumMatrix>(env, compressors);
    qjS = NumNumMatrix(env, compressors);
    cjS = NumNumMatrix(env, compressors);
    
    ClientServiced = IloRangeArray(env, wells, 1, 1);
    FacilityAssignment = IloRangeArray(env, compressors, 0, 1); 
	model.add(ClientServiced);
	model.add(FacilityAssignment);

	BranchConst = IloArray<IloRangeArray> (env, compressors); //Need for the 2nd branch level 
	for (int j = 0; j < compressors; j++){
		BranchConst[j] = IloRangeArray(env, wells, 0, 1);		
		model.add(BranchConst[j]);
	}
      
}

void pricing::init(IloEnv& env, const Instance& inst, const IloInt& j){
	//~ cplex.setParam(IloCplex::NumericalEmphasis, 1);    //PrecisÃ£o numÃ©rica	
	//~ cplex.setParam(IloCplex::AdvInd, 2); //Advanced basis
	//~ cplex.setParam(IloCplex::EpGap, 0.01);
	//~ cplex.setParam(IloCplex::RootAlg, IloCplex::Dual);
	cplex.setOut(env.getNullStream());
	//~ cplex.setParam(IloCplex::RandomSeed, 123456789);
	cplex.setParam(IloCplex::Threads, 1);
	cplex.setParam(IloCplex::ClockType, 2);
	int i, ka;
	int n = inst.compressors;
	int m = inst.wells;
	int k = inst.k;
		
	//Variables
	x = IloNumVarArray(env, m, 0, 1, ILOINT);
	q = IloNumVar(env);//, 0, IloInfinity,ILOFLOAT);
	
	///Formulation with SOS variables	
	#ifndef NUSES0S2 
	lambdaL = IloNumVarArray(env, k, 0.0, 1.0, ILOFLOAT);
	lambda = IloSOS2(env, lambdaL);	
	
	/*** Objective ***/
	IloExpr expr(env);
	IloExpr gasDemand(env);	
	expr.clear();
	gasDemand.clear();
	for (i = 0; i < m; i++){
		if(inst.orac[j][i] == 1){
			expr += x[i];
			//Linear combinations constraint
			model.add(q + (inst.qCMax[j] - inst.qMaxIJ[j][i])*x[i] <= inst.qCMax[j]);

			gasDemand += inst.qW[i]*x[i];			
		}
	}	
	expr += IloScalProd(inst.pointsH[j] , lambdaL);
		
	SPj.setExpr(expr);
	model.add(SPj);
	
	/***Constraints***/
	//Piecewise gas rate
	model.add(q == IloScalProd(inst.pointsQ[j] , lambdaL));
	
	//Gas rate capacity
	model.add(q == gasDemand);
	//~ model.add(q >= gasDemand);		
	
	//Picewise one segment per compressor pressure level curve
	model.add(IloSum(lambdaL) == 1);		
	model.add(lambda);
	#endif
	
	///Formulation with binary variables
	#ifndef NUSEBINARY
	lambdaL = IloNumVarArray(env, k-1, 0, 1.0, ILOFLOAT);
	lambdaR = IloNumVarArray(env, k-1, 0, 1.0, ILOFLOAT);
	z = IloNumVarArray(env, k-1, 0, 1, ILOINT);
	
	/*** Objective ***/
	IloExpr expr(env);		
	IloExpr gasRate(env);		
	IloExpr gasDemand(env);		
	expr.clear();
	gasRate.clear();
	gasDemand.clear();
	

	for (i = 0; i < m; i++){
		if(inst.orac[j][i] == 1){
			expr += x[i];
			//Linear combinations constraints
			model.add(q + (inst.qCMax[j] - inst.qMaxIJ[j][i])*x[i] <= inst.qCMax[j]);		
			//Demand
			gasDemand += inst.qW[i]*x[i];
		}
	}
	for (ka = 1; ka < k; ka++){ 
		expr += (inst.pointsH[j][ka-1]*lambdaL[ka-1] + inst.pointsH[j][ka]*lambdaR[ka-1]);
		//Piecewise gas rate	
		gasRate += (inst.pointsQ[j][ka-1]*lambdaL[ka-1] + inst.pointsQ[j][ka]*lambdaR[ka-1]);
		//Picewise sum of leftCoef and right coef of the segment
		model.add(lambdaL[ka-1]+lambdaR[ka-1] == z[ka-1]);
	}
	SPj.setExpr(expr);
	model.add(SPj);
	model.add(q == gasRate);
	
	/***Constraints***/
	//Gas rate capacity	
	//~ model.add(IloScalProd(inst.qW,x) <= q);		//Need to put all variables X for get the value to the column
	//~ model.add(IloScalProd(inst.qW,x) == q);
	model.add(q == gasDemand);
	//~ model.add(q >= gasDemand);

	//Picewise one segment per compressor pressure level curve
	model.add(IloSum(z) == 1);	
	#endif	
}
void pricing::setDualCoef(IloEnv& env, const IloInt& j, const IloNum& miD, const IloNumArray& piD, const IloNumArray& psyD, const Instance& inst, const IloNumArray& coef){		
	for (unsigned int i = 0; i < inst.wells ; i++){		
		if (inst.orac[j][i] == 1){						
			SPj.setLinearCoef(x[i], inst.cIJ[j][i] - piD[i] - psyD[i]);
		}		
	}	
	SPj.setConstant(inst.cJ[j] - miD);	
}

void CGModel::greedyIntialColumns(IloEnv& env, const Instance& inst){ //O(nm)
	int i, j, c;
	//Define the artificial variable structure
	IloNum cjA = pow(IloSum(inst.cJ),2);
	IloNum qjA = IloSum(inst.qCMax);
	IloNumArray deltaA(env, inst.wells);

	string str = "";
	char const* name;

	for(j = 0; j < inst.compressors; j++){
		delta[j] = NumNumMatrix(env);
		cjS[j] = IloNumArray(env);
		qjS[j] = IloNumArray(env);
		lambda[j] = IloNumVarArray(env);
		
		#ifndef NInitialColumns
		delta[j].add(IloNumArray(env, inst.wells));
		cjS[j].add(0);		
		qjS[j].add(0);

		IloNum qmin = 100000.0;
		c = delta[j].getSize()-1;
		
		for (i = 0; i < inst.wells; i++){
			deltaA[i] = 1;
			if (inst.orac[j][i] >= NEAR_ONE){
				if ((qjS[j][c]+inst.qW[i]) <= inst.qCMax[j] && (qjS[j][c]+inst.qW[i]) <= inst.qMaxIJ[j][i] && (qjS[j][c]+inst.qW[i]) <= qmin){
					delta[j][c][i] = 1;
					qjS[j][c] += inst.qW[i];
					if (inst.qMaxIJ[j][i] < qmin){
						qmin = inst.qMaxIJ[j][i];
					}				
				}else{
					//Add the column to the model
					//name = 'xtype*j_col'
					str = "x1*" + to_string(j) + "_" + to_string(c);
					name = str.c_str();			
					//Verificar se o qjS < qCmin
					if (qjS[j][c] < inst.qCMin[j]){				
						qjS[j][c] = inst.qCMin[j];			
					}			
					cjS[j][c] = costColum(inst.wells, delta[j][c], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][c], getPressure(inst, j, qjS[j][c]));

					lambda[j].add( cost(cjS[j][c]) +  ClientServiced(delta[j][c]) + FacilityAssignment[j](1) + BranchConst[j](delta[j][c]) );
					lambda[j][c].setName(name);	
					
					//Creates a new column
					delta[j].add(IloNumArray(env, inst.wells));
					qjS[j].add(inst.qW[i]);
					cjS[j].add(0);
					c = delta[j].getSize()-1;
					delta[j][c][i] = 1;								
					qmin = inst.qMaxIJ[j][i];				
				}
			}			
		}
		//Add the last column		
		//name = 'xtype*j_col'
		str = "x1*" + to_string(j) + "_" + to_string(c);
		name = str.c_str();			
		//Verificar se o qjS < qCmin
		if (qjS[j][c] < inst.qCMin[j]){				
			qjS[j][c] = inst.qCMin[j];			
		}			
		cjS[j][c] = costColum(inst.wells, delta[j][c], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][c], getPressure(inst, j, qjS[j][c]));

		lambda[j].add( cost(cjS[j][c]) +  ClientServiced(delta[j][c]) + FacilityAssignment[j](1) + BranchConst[j](delta[j][c]) );
		lambda[j][c].setName(name);	
		#endif		
		
		///Slack variables (to ensure initial feasible solution for RMP)
		for (i = 0; i < inst.wells; i++){
			deltaA[i] = 1;
		}
		delta[j].add(deltaA);
		cjS[j].add(cjA);
		qjS[j].add(qjA);
		c = delta[j].getSize()-1; //'c' is the last index of the column (in the case, the artificial)

		lambda[j].add( cost(cjS[j][c]) +  ClientServiced(delta[j][c]) + FacilityAssignment[j](1) + BranchConst[j](delta[j][c]));
		str = "x0*" + to_string(j) + "_" + to_string(c);
		name = str.c_str();						
		lambda[j][c].setName(name);

		///-Print column
		//~ for (int col = 0; col <= c; col++){
			//~ cout << lambda[j][col].getName() << " j " << j  << " Col " << col;
			//~ for (i = 0; i < inst.wells; i++){
				//~ cout << " " << delta[j][col][i];
			//~ }
			//~ cout << " qj " << qjS[j][col] << " cJ " << cjS[j][col] << endl;
		//~ }
		///-		
	}	
}

void CGModel::addColumn(IloEnv env, const IloInt& j, const IloNum& columnCost, const IloNumArray& coef, const double& qJ, const Instance& inst){ //O(1)
	delta[j].add(coef);
	//~ delta[j].add(IloNumArray(env, inst.wells));
	//~ for (int i = 0; i < inst.wells; i++){
		//~ delta[j][delta[j].getSize()-1][i] = coef[i];
	//~ }
	
	//~ cjS[j].add(floor(columnCost * DecPrecision) / DecPrecision);
	cjS[j].add(columnCost);
	//~ qjS[j].add(floor(qJ * DecPrecision) / DecPrecision);
	qjS[j].add(qJ);
	
	//Name construct
	string s =  "x1*" + to_string(j) + "_" + to_string(cjS[j].getSize()-1);
	char const* name = s.c_str(); 
	
	#ifndef NCOUT
	cout << "Add " << s << " " << columnCost << " " ;
	for (int i = 0; i < coef.getSize(); i++) cout << coef[i] << " ";
	cout << endl;
	
	for (int i = 0; i < coef.getSize(); i++){
		if (inst.orac[j][i] == 0 && coef[i] >= NEAR_ONE) cout << "J: " << j+1 << " I: " << i+1 << " cIJ " << inst.cIJ[j][i] << " qCmaxIJ " << inst.qMaxIJ[j][i] << endl;
	}
	#endif
	
	//Objective and constraints 5b/5c/5d		
	lambda[j].add(cost(columnCost) + ClientServiced(coef) + FacilityAssignment[j](1) + BranchConst[j](coef));
	//Add name	
	lambda[j][lambda[j].getSize()-1].setName(name);
}

int CGModel::getStatus(const IloNum& n){ // O(nc) when integral = theta(nc)
	//* 1 if is fractional
	//* 2 if is integral.
	//* 3 if is infeasible (uses artificial variables)
	int j, c, result;
	result = 2;
	for(j = 0; j < n; j++){
		for(c = 0; c < lambda[j].getSize(); c++){
			string varName = lambda[j][c].getName();
			//~ cout << "Name " << varName << " " << j << " " << c << "Value " << cplex.getValue(lambda[j][c]) << endl;
			int varType = atoi(varName.substr(1, 1).c_str());			
			if (varType == 0){
				if(cplex.getValue(lambda[j][c]) > RC_UB){
					result = 3;
					//~ cout << varName << " artificial" << endl;
					goto ex;
				}
			}
			if (cplex.getValue(lambda[j][c]) > RC_UB && cplex.getValue(lambda[j][c]) < NEAR_ONE){			
				result = 1;
				//~ goto ex;
			}
		}		
	}
	ex:	return result;
}

bool CGModel::cg_iter(IloEnv& env, const Instance& inst, IloArray<csp::pricing>& subproblem, Timer<chrono::milliseconds>& timer_ms, float& mp_time, 
float& p_time, const nodeBP& node, const double& UB, int& cgIter, int& cpxNodes, int& early_branch, const Timer<chrono::milliseconds>& timer_global, 
	const double& timeLimit, const IloIntArray& seeds, const int& nodes){
	timer_ms.start();
	if(!cplex.solve()){ //This never happen in the first iteration of B&P because the slack variables
		mp_time += timer_ms.total();					
		return false;
	}
	cpxNodes += cplex.getNnodes();
	double objVal = cplex.getObjValue();
	///--When the new constraints doesn't affect the obj function (RHS = % of increase in ObjValue from previous to current node)
	#ifndef NEarlyBranch
	double diff_prev_node = ((objVal * 100)/node.lb) - 100;
	if (diff_prev_node < 0.1){ 
		//~ cout << "Branching again...\n Previous node: " << node.lb << " Current node: " << objVal << " - " << diff_prev_node << "% great\n";
		early_branch++;
		return true;
	}
	#endif
	///--
	mp_time += timer_ms.total();	
	int cont; //Controla o nÂº de colunas adicionadas ao master
	NumNumMatrix coef(env, inst.compressors); 
	cout.precision(dbl::max_digits10); //Max precision in the COUT
	int i,j;
	int n = inst.compressors;
	int m = inst.wells;
	int nLahc = 0;
	
	IloNumArray pi(env, m);
	IloNumArray mi(env, n);
	NumNumMatrix psy(env, n);

	//Vars to calculate the real reduced cost
	double qJ, pJ, cCost, redCost, columnCostP;
	IloNumArray lamb(env, inst.k);
	for(;;){
		cgIter++;
		//Get dual variables for the relaxed solution
		cplex.getDuals(pi, ClientServiced); //5b
		cplex.getDuals(mi, FacilityAssignment); //5c
		for (j = 0; j < n; j++){ 
			psy[j] = IloNumArray(env, m); 	
			cplex.getDuals(psy[j] , BranchConst[j]);  //Get duals of 5d (compressor j)			
		}		
		IloNumArray pricing_redCost(env, n);
		#ifndef NCuts
		double least_rc = 1000;
		#endif
		cont = n;		
		#ifdef TWOPHASE
		for(j = 0; j < n; j++){
			if (FacilityAssignment[j].getUB() >= RC_UB){ //Execute the pricing only if the compressor is allowed to be in the solution								
				///Execute pricing - First phase
				timer_ms.start();
				Solution s = getReducedCost(env,inst,44,j,mi[j],pi,psy[j],seeds,cgIter, nodes, subproblem, 22, 20);
				p_time +=  timer_ms.total();
				if (s.rc<RC_LB){ //Add column
					addColumn(env, j, s.c, s.x, s.q, inst); 					
					cont--;
					nLahc++;
					#ifndef NCuts
					if (s.rc < least_rc) least_rc = s.rc;
					#endif
				}					
			}
		}		
		#endif
		if (cont == n){ ///Second phase - applies MIP only if no columns were found by first phase
			for(j = 0; j < n; j++){
				if (FacilityAssignment[j].getUB() >= RC_UB){ //Execute the pricing only if the compressor is allowed to be in the solution
					coef[j] = IloNumArray(env, inst.wells);
					subproblem[j].setDualCoef(env, j, mi[j], pi, psy[j], inst, delta[j][delta[j].getSize()-1]);			
					subproblem[j].cplex.setParam(IloCplex::TiLim, timeLimit - (timer_global.total()/1000));	
						
					timer_ms.start();
					if(subproblem[j].cplex.solve()){
						p_time +=  timer_ms.total();
						cpxNodes += subproblem[j].cplex.getNnodes();																					
						pricing_redCost[j] = subproblem[j].cplex.getObjValue();					
						if (pricing_redCost[j] < RC_LB){						
							///Recalculate reducedCost																				
							for (int i = 0; i < inst.wells; i++){
								if (inst.orac[j][i] == 1) coef[j][i] = subproblem[j].cplex.getValue(subproblem[j].x[i]);
							}
							qJ = subproblem[j].cplex.getValue(subproblem[j].q);							
							//~ qJ = floor(qJ * DecPrecision) / DecPrecision; //Only need if q >= sum qWi
							pJ = getPressure(inst, j, qJ);
							cCost = costColum(inst.wells, coef[j], j, inst.cJ, inst.cIJ, inst.dJ, qJ, pJ); // Cost of recalculated column
							redCost = cCost - mi[j] - IloScalProd(coef[j], pi) - IloScalProd(coef[j], psy[j]); //Reduced cost of recalculated column
							columnCostP = pricing_redCost[j] + mi[j] + IloScalProd(coef[j], pi) + IloScalProd(coef[j], psy[j]); // Cost of pricing column
							//~ cout << pricing_redCost[j] << " " << redCost << endl;
							if (redCost < RC_LB){
								///To use the aproximated value of the column 
								//~ addColumn(env, j, columnCostP, coef[j], qJ, inst); //Coluna do Pricing
								///To use the most precise value of the column 
								addColumn(env, j, cCost, coef[j], qJ, inst); //Coluna recalculada				
								cont--;
								#ifndef NCuts
								if (pricing_redCost[j] < least_rc) least_rc = pricing_redCost[j]; //Get the least reduced cost of the pricing in the iteration
								#endif													
							}
						}					
					}else p_time +=  timer_ms.total();	
				}				
			}
		}if(cont == n) break; //Caso nenhuma coluna seja adicionada, i.e. todos SPj sÃ£o >=0
		else{
			#ifndef NCuts
			if (least_rc * n + objVal > UB){ //Pre-calc of the LB to cutoff the B&P tree												
				return false;				
			}
			#endif
			timer_ms.start();
			cplex.solve();	//Never will be infeasible			
			cpxNodes += cplex.getNnodes();											
			//~ if (cplex.getObjValue() > UB) return false; //If the obj is greater than the UB of B&P
			mp_time +=  timer_ms.total();				
			objVal = cplex.getObjValue();
		}		
	}
	//~ cout << "Add columns by LAHC " << nLahc << endl;
	return true;
}

void csp::cg(string file, const int& n_ini, const int& n_fim, const double& timeLimit) {
	stringstream out;
	out << 3 << "_Inst_" << n_ini << "_to_" << n_fim << "_" << timeLimit << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	
	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj\tIter \t CpxNods" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}
		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime{0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}						
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);

			double ub = {numeric_limits<double>::max()};
			
			nodeBP node(env, 0,0,0,1);
			int cgIt = 0;
			int cplexNodes = 0;
			int early_branch = 0;
			
			//Build a random array for the LAHC - TODO - use it as parameter for B&P	
			IloIntArray seeds(env, 1000);
			#ifdef TWOPHASE
			for (int ds=0; ds < seeds.getSize(); ds++){
				seeds[ds]=iRand(0,9999,ds);		
			}
			#endif
			
			//Solve root node			
			cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node, ub, cgIt, cplexNodes, early_branch, timer_global, timeLimit, seeds,0);	
						
			totalTime += timer_global.total();
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << cgModel.cplex.getObjValue() << "\t" << cgIt << "\t" << cplexNodes << "\t";
			
			int cg_status = cgModel.getStatus(inst.compressors);
			if (cg_status == 1) output << "Feasible" << endl;
			else if (cg_status == 2) output << "Optimal" << endl;
			else output << "Infeasible" << endl;
			double sumCols = 0;
			//~ //Print columns
			//~ for (int j = 0; j < inst.compressors; j++){
				//~ sumCols += cgModel.lambda[j].getSize();
				//~ for (int c = 0; c < cgModel.lambda[j].getSize(); c++){
					//~ if (cgModel.cplex.getValue(cgModel.lambda[j][c]) > RC_UB){
						//~ cout << j << " >> " << cgModel.cplex.getValue(cgModel.lambda[j][c]) <<  "* [ ";
						//~ for (int i = 0 ; i < inst.wells; i++){
							//~ cout << " " << cgModel.delta[j][c][i];
						//~ }
						//~ double realCJs = inst.cJ[j] + IloScalProd(inst.cIJ[j], cgModel.delta[j][c]) + inst.dJ[j] * cgModel.qjS[j][c] * cgModel.getPressure(inst, j, cgModel.qjS[j][c]);
						//~ cout << "] cJs: " << cgModel.cjS[j][c] << "/real " << realCJs << "  qJ: " << cgModel.qjS[j][c] << " pJ: " << cgModel.getPressure(inst, j, cgModel.qjS[j][c]) << endl;
					//~ }
				//~ }
			//~ }
			//~ output << "\t" << sumCols << endl;
			//~ cout << cgModel.cplex.getObjValue() << endl;
		}
		catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
