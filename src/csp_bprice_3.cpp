#include <queue>          
#include <vector>
#include <limits>
#include <math.h>
#include <algorithm>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"
#define NDEBUG
#include <assert.h>

//~ #define branch2strategy2
#define RMPZ			
//~ #define LocalSearch
//~ #define BestImprovment
#define NStorageSolution

#define RC_LB -1.0e-6
#define RC_UB 1.0e-5
#define NEAR_ONE 0.99999
#define DELIMITER "_"
#define M 1.0e+8

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using namespace csp;
			
int cuts = 0, ins; //ins is the index of the instance
int nodes = 0;

//For rules in solving RMP(Z)
int lastNodeUpdate;
double coefNodeRmp;
int solveAtEachNodes;

int cgIt = 0;
int n_cols;
int n_colsF;
int depth;
int max_depth;
double DecPrecision = 1.0e+6;
float time_root{0};
int cplexNodes;
int early_branch;
IloNum pUb;
double timeL;
//Storage the branch rules of the B&P
IloIntArray branchRuleL1;
IloArray<IloIntArray> branchRuleL2;

bool optimal = false;

void CGModel::getColumnIndices(const IloNumVar& var, int& j, int& c, int& type){
	string varName = var.getName();
	type = atoi(varName.substr(1,1).c_str());
	j = atoi(varName.substr(3, varName.find(DELIMITER)).c_str());
	c = atoi(varName.substr(varName.find(DELIMITER)+1).c_str());	
}

void CGModel::findCloseIntegralityLevel1(IloEnv& env, const Instance& inst, double& varValue, int& compressor, const IloNumArray& sum_colJ){
	int jMin=-1, jMax=-1;
	int c;
	double maxV = RC_UB, minV = NEAR_ONE;
	//Define the 'j's with value closer to 1 and 0
	for(int j = 0; j < inst.compressors; j ++){
		if (sum_colJ[j] < NEAR_ONE && sum_colJ[j] > RC_UB){
			if (sum_colJ[j] >= maxV){ //Closer to 1
				maxV = sum_colJ[j];
				jMax = j;
			}else if (sum_colJ[j] <= minV){//Closer to 0
				minV = sum_colJ[j];
				jMin = j;
			}
		}
	}
	//Define between jMin and jMax the most close to integrality
	if (1.0 - (floor(minV * DecPrecision) / DecPrecision) > (floor(maxV * DecPrecision) / DecPrecision)){		
		varValue = minV;
		compressor = jMin;	
	}else{		
		varValue = maxV;
		compressor = jMax;		
	}	
}

//To find other column of the same compressor when all lines set as 1 were restricted
void CGModel::findOtherSameCompressor(const Instance& inst, double& varValue, const int& compressor, const int& origCol, int& newCol){
	//~ int type, col, j;
	for(int c = 0; c < relaxModelValues[compressor].getSize(); c++){
		if(relaxModelValues[compressor][c] > RC_UB && relaxModelValues[compressor][c] < NEAR_ONE){
			if(c != origCol){						
				for(int i = 0; i < inst.wells; i++){
					if (delta[compressor][c][i] >= NEAR_ONE && BranchConst[compressor][i].getLB() < NEAR_ONE){
						newCol = c;					
						varValue = relaxModelValues[compressor][c];
						goto founded;
					}
				}
			}
		}
	}
	founded: return;
}
#ifndef branch2strategy2 //Strategy for branch 2: Find the most close to integrality lambda and define a well of it.
void CGModel::findCloseIntegralityLevel2(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well, const NumNumMatrix& sum_colIJ){
	int jMin=-1, jMax=-1;			//To identify the compressor of the most two 'integral' variable
	int j, c;						//Iterators
	int colMin, colMax, col, col1; 	//Indexes and idenficators of the column of 'j' above;
	int typeMin, typeMax, type; 	//Identificators of type of variable
	double maxV = RC_UB, minV = NEAR_ONE;	
	for(j = 0; j < inst.compressors; j++){
		for(c = 0; c < relaxModelValues[j].getSize(); c++){			
			if(relaxModelValues[j][c] >= RC_UB && relaxModelValues[j][c] <= NEAR_ONE){
				//Printing
				//~ cout << "j " << j << " c " << c << " value " << relaxModelValues[j][c] << " ";
				//~ for (int i=0;i<inst.wells;i++) cout << delta[j][c][i] << " ";
				//~ cout << endl;
				
				if (relaxModelValues[j][c] > maxV){ //If closer to 1				
					maxV = relaxModelValues[j][c];					
					jMax = j;	
					colMax = c;	//Assuming that	the index of columns are the same in lambda and delta/cJS
				}else if (relaxModelValues[j][c] < minV){ //If closer to 0
					minV = relaxModelValues[j][c];					
					jMin = j;
					colMin = c;					
				}
			}			
		}	
		//printing
		//~ cout << "Branch rule " << j << " " ;
		//~ for (int i = 0; i <inst.wells; i++){
			//~ cout << branchRuleL2[j][i] << " ";
		//~ }
		//~ cout << endl;
	}	
	if (1.0 - (floor(minV * DecPrecision) / DecPrecision) >= (floor(maxV * DecPrecision) / DecPrecision)){	
		varValue = minV;
		compressor = jMin;
		col = colMin;
		type = typeMin;
	}else{
		varValue = maxV;
		compressor = jMax;
		col = colMax;
		type = typeMax;
	}	
	//~ cout << "most close to integrality: " << compressor << "_" << col << endl;
	col1 = col;
	//Find the well which is supplied by the 'compressor' in the column 'col' and is not restricted in the pricing yet
	findWell:		
	for (int i = 0; i < inst.wells; i++){			
		if(delta[compressor][col1][i] >= NEAR_ONE && BranchConst[compressor][i].getLB() == 0 && sum_colIJ[compressor][i] > RC_UB && sum_colIJ[compressor][i] < NEAR_ONE){
			well = i;
			goto fim;
		}
	}	
	
	//If no well was found		
	//~ cout << "NAO ACHOU POÇO: " << compressor << " " << col1 << endl;	
	//~ findOtherSameCompressor(inst, varValue, compressor, col, col1);
	if (varValue > 0.5){
		varValue = minV;
		compressor = jMin;
		col1 = colMin;
		type = typeMin;		
	}else{
		varValue = maxV;
		compressor = jMax;
		col1 = colMax;
		type = typeMax;
	}
	goto findWell;
	fim: return;	// cout << " Well " << well << endl; 
}
#endif
//Search for a well "most supplied" by a compressor
#ifdef branch2strategy2
void CGModel::findCloseIntegralityLevel2(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well, const NumNumMatrix& sum_colIJ){
	int jMin=-1, jMax=-1;			
	int iMin=-1, iMax=-1;			
	int j, i;						
	int colMin, colMax, col, col1; 	
	int typeMin, typeMax, type; 	
	double maxV = RC_UB, minV = NEAR_ONE;	
	for(j = 0; j < inst.compressors; j++){
		for(i = 0; i < inst.wells; i++){			
			if(sum_colIJ[j][i] >= RC_UB && sum_colIJ[j][i] <= NEAR_ONE){
				if (sum_colIJ[j][i] > maxV){ //If closer to 1				
					maxV = sum_colIJ[j][i];
					jMax = j;
					iMax = i;					
				}else if (sum_colIJ[j][i] < minV){ //If closer to 0
					minV = sum_colIJ[j][i];
					jMin = j;
					iMin = i;	
				}
			}			
		}	
	}

	if (1.0 - (floor(minV * DecPrecision) / DecPrecision) >= (floor(maxV * DecPrecision) / DecPrecision)){
		varValue = minV;
		compressor = jMin;
		well = iMin;
	}else{
		varValue = maxV;
		compressor = jMax;
		well = iMax;
	}
	
	//~ cout << "Min: " << minV << " Max " << maxV << endl;
}
#endif
	
bool CGModel::canSwap(const Instance& inst, const int& i, const int& i1, const IloIntArray& solutionWell, const IloNumArray& gasRate, const IloNumArray& maxGasRate){
	if (solutionWell[i] == solutionWell[i1]) return false; //Same compressor suppling both wells;
	if (inst.orac[solutionWell[i]][i1] == 0 || inst.orac[solutionWell[i1]][i] == 0) return false; // if the compressor of well i is able to supply well i1 and vice-e-versa
	if (gasRate[solutionWell[i]]  - inst.qW[i]  + inst.qW[i1] > maxGasRate[solutionWell[i]]) return false; // if the compressor of well i hasn't capacity for well i1
	if (gasRate[solutionWell[i1]] - inst.qW[i1] + inst.qW[i]  > maxGasRate[solutionWell[i1]]) return false; // if the compressor of well i1 has capacity for well i
	if (gasRate[solutionWell[i]]  - inst.qW[i] +  inst.qW[i1] > inst.qMaxIJ[solutionWell[i]][i1]) return false; //if the compressor of well i has capacity for maxOutupu in well i1
	if (gasRate[solutionWell[i1]] - inst.qW[i1] + inst.qW[i] > inst.qMaxIJ[solutionWell[i1]][i]) return false; //if the compressor of well i1 has capacity for maxOutupu in well i

	return true;
}
//Return the operation cost of swapping well i from compressor j to j1 and the well i1 from compressor j1 to j
double CGModel::swapCost(const Instance& inst, const int& i, const int& i1, //Well info
					const IloIntArray& solutionWell, const IloNumArray& gasRate, const IloNumArray& operCosts){ //Compressor info
	int j = solutionWell[i];
	int j1 = solutionWell[i1];
	double new_gas_rate_j = gasRate[j] - inst.qW[i] + inst.qW[i1];
	double new_gas_rate_j1 = gasRate[j1] - inst.qW[i1] + inst.qW[i];
	double new_cost_j = inst.dJ[j] * new_gas_rate_j * getPressure(inst, j, new_gas_rate_j) + inst.cIJ[j][i1];
	double new_cost_j1 = inst.dJ[j1] * new_gas_rate_j1 * getPressure(inst, j1, new_gas_rate_j1) + inst.cIJ[j1][i];
	return new_cost_j + new_cost_j1;
}

void CGModel::swap(const Instance& inst, const int& i, const int& i1, IloIntArray& solutionWell,
IloNumArray& gasRate, IloNumArray& maxGasRate, IloNumArray& operCosts, double& obj){
	int j = solutionWell[i];
	int j1 = solutionWell[i1];
	
	//Update gasRate
	gasRate[j] = gasRate[j] - inst.qW[i] + inst.qW[i1];
	gasRate[j1] = gasRate[j1] - inst.qW[i1] + inst.qW[i];
	
	//Remove the value of obj corresponding to the current cost	
	obj = obj - (operCosts[j] + operCosts[j1] + inst.cIJ[j][i] + inst.cIJ[j1][i1]);
	
	//Update operations cost
	operCosts[j]  = inst.dJ[j]  * gasRate[j]  * getPressure(inst, j,  gasRate[j]);
	operCosts[j1] = inst.dJ[j1] * gasRate[j1] * getPressure(inst, j1, gasRate[j1]);
	
	//Update obj
	obj = obj + operCosts[j] + operCosts[j1] + inst.cIJ[j][i1] + inst.cIJ[j1][i];		

	//Swap the indexes of the solution		
	solutionWell[i] = j1;
	solutionWell[i1] = j;
	
	//Update the maxGasRate of j and j1
	double minJ = inst.qCMax[j], minJ1 = inst.qCMax[j1];
	for (int w = 0; w < inst.wells; w++){		
		if (solutionWell[w] == j){
			if (inst.qMaxIJ[j][w] < minJ){
				minJ = inst.qMaxIJ[j][w];
			}
		}
		if (solutionWell[w] == j1){
			if (inst.qMaxIJ[j1][w] < minJ1){
				minJ1 = inst.qMaxIJ[j1][w];
			}
		}
	}
	maxGasRate[j] == minJ;
	maxGasRate[j1] == minJ1;
}
double CGModel::greedyWSC(const Instance& inst, IloEnv& env){
	int j, c, i;
	IloIntArray A(env, inst.wells); //Identify how many times each wells is covered
	int coveredWells = 0; //Identify the number of covered wells
	NumNumMatrix An(env, inst.wells); // Ifentify which compressor(s) supply each well (if size = 1, just 1 compressor supply)
	for (i = 0; i < inst.wells; i++) An[i] = IloNumArray(env);
	IloIntArray N(env, inst.compressors); //Identify the activated compressors through the index of column 	(-1 not used)
	NumNumMatrix eCost(env, inst.compressors); //Identify for each column it effective cost
	NumNumMatrix supplyW(env, inst.compressors); //Identify for each column the # of wells that can be add and are not in A
	IloNumArray qjc(env, inst.compressors); //Storage the gas rate of the selected columns
	IloNumArray cjc(env, inst.compressors); //Sotrage the column cost of each selected compressor
	
	double objValue = 0;
	//Initialization and first step
	for (j = 0; j < inst.compressors; j++){ 
		N[j] = -1;
		eCost[j] = IloNumArray(env, lambda[j].getSize());
		supplyW[j] = IloNumArray(env, lambda[j].getSize());
		for (c = 0; c < eCost[j].getSize(); c++){
			for (i = 0; i < inst.wells; i++){				
				if (delta[j][c][i] >= NEAR_ONE){ //If i is covered by column C 
					supplyW[j][c]+=1;					
				}
			}
			if (supplyW[j][c] > 0){
				eCost[j][c] = cjS[j][c]/supplyW[j][c];
			}else{
				eCost[j][c] = M;
			}
		}		
	}
	//Enters loop
	do{
	//Identify the min eCost
	double minEc = M;
	int jMin, cMin;
	for (j = 0; j < inst.compressors; j++){
		if (N[j] == -1){ //If the compressor is not installed in the partial solution A			
			for (c = 0; c < eCost[j].getSize(); c++){
				if (eCost[j][c] < minEc){
					minEc = eCost[j][c];
					jMin = j;
					cMin = c;
				}
			}
		}
	}
	//Add the min eCost
	objValue += cjS[jMin][cMin];	
	N[jMin] = cMin;
	qjc[jMin] = qjS[jMin][cMin];
	cjc[jMin] = cjS[jMin][cMin];
	for (i=0;i<inst.wells;i++){		
		if (delta[jMin][cMin][i] >= NEAR_ONE){
			if (A[i] <= RC_UB) coveredWells++;
			A[i] += 1;
			An[i].add(jMin);
			
		}
	}
	
	//Update the effective costs 
	for (j = 0; j < inst.compressors; j++){ 
		supplyW[j] = IloNumArray(env, lambda[j].getSize()); //Reset the supplyW values
		if (N[j] == -1){ //If the compressor is not installed in the partial solution A			
			for (c = 0; c < eCost[j].getSize(); c++){
				eCost[j][c] = M;
				for (i = 0; i < inst.wells; i++){
					if (delta[j][c][i] >= NEAR_ONE && A[i] < RC_UB){ //If i is covered by column c and not covered in A
						supplyW[j][c] += 1;					
					}
				}
				if (supplyW[j][c] > 0){
					eCost[j][c] = cjS[j][c]/supplyW[j][c];
				}
			}
		}		
	}	
	
	}while(coveredWells < inst.wells);
	for (i = 0; i < inst.wells; i++){
		if (A[i] > 1){ //If a wells is supplied twice or more
			double minCij = M;
			int minComp = -1;
			for (int l = 0; l < An[i].getSize(); l++){ //Verify the min cij
				if (inst.cIJ[An[i][l]][i] < minCij){
					minCij = inst.cIJ[An[i][l]][i];
					minComp = An[i][l];
				}
			}
			//Remove i for others columns except minComp
			for (int l = 0; l < An[i].getSize(); l++){
				if (An[i][l] != minComp){
					IloNumArray delta1(env, inst.wells);
					qjc[An[i][l]] = 0;
					cjc[An[i][l]] = 0;
					for (int m = 0; m < inst.wells; m++){
						if (delta[An[i][l]] [N[An[i][l]]] [m] >= NEAR_ONE){
							qjc[An[i][l]] += inst.qW[m];
							delta1[m] = 1;
						}
					}
					double pressure = getPressure(inst, An[i][l], qjc[An[i][l]]);
					cjc[An[i][l]] = costColum(inst.wells, delta1, An[i][l], inst.cJ, inst.cIJ, inst.dJ, qjc[An[i][l]], pressure);
					
				}
			}
		}
	}
	
	return IloSum(cjc);
}
double CGModel::setPartitioningMaster(const Instance& inst, IloEnv& env, const int& iter){		
	double z;
	//Creates the main matrixes in the first call of the method 
	if (iter == 1){
		matrixDelta = NumNumMatrix(env);
		matrixCost = IloNumArray(env);
		lastNumCol = IloIntArray(env, inst.compressors);
		//First n positions are for the sets where the compressor is not selected, storage the number of columns of each compressor
		for (int i = 0; i < inst.compressors; i++){
			IloNumArray col(env,inst.compressors+inst.wells);
			col[i] = 1;
			matrixDelta.add(col);
			matrixCost.add(0);			
			lastNumCol[i] = delta[i].getSize();			
		}
		//For each compressor feeds the the matrix
		for (int j = 0; j < inst.compressors; j++){
			for (int c = 0; c < delta[j].getSize(); c++){
				IloNumArray col(env,inst.compressors+inst.wells);
				col[j] = 1;
				for (int i = inst.compressors; i < col.getSize(); i++){
					col[i] = delta[j][c][i-inst.compressors];					
				}
				matrixCost.add(cjS[j][c]);
				matrixDelta.add(col);
				
				//~ cout << matrixCost[matrixCost.getSize()-1] << " \t" << col << "\n \n";
			}
		}		
	//Feed the matrix in the others iterations
	}else{
		for (int j = 0; j < inst.compressors; j++){
			//If new columns were add for the compressor j
			if (lastNumCol[j] < delta[j].getSize()){				
				for (int c = lastNumCol[j]; c < delta[j].getSize(); c++){
					IloNumArray col(env,inst.compressors+inst.wells);
					col[j] = 1;
					for (int i = inst.compressors; i < col.getSize(); i++){
						col[i] = delta[j][c][i-inst.compressors];
					}
					matrixCost.add(cjS[j][c]);
					matrixDelta.add(col);
				}
			//Update last columns add
			lastNumCol[j] = delta[j].getSize();			
			}			
		}
	}
		
	return z;
	
}

double CGModel::primalHeuristic(const Instance& inst, IloEnv& env){	
	struct Column{
		int j;
		int col;		
		double relaxValue;
	};
	struct CompareColumn{
		public:
		bool operator()(const Column& c1,const Column& c2){
			return (c2.relaxValue > c1.relaxValue);
		}
	};
	//To storage the wells that was nos supplied yet
	struct Well{
		int i;
		double demand;
	};
	struct CompareWell{
		public:
		bool operator()(const Well& w1,const Well& w2){
			return (w1.demand > w1.demand);
		}
	};	

	priority_queue<Well,vector<Well>, CompareWell> pQW;
	IloNumArray gasRate(env, inst.compressors); 
	IloNumArray operCosts(env, inst.compressors); 
	IloNumArray maxGasRate(env, inst.compressors); 
	IloIntArray solutionWell(env, inst.wells); // Indicate the index compressor that  supply the well
	for (int i = 0; i < inst.wells; i++){
		solutionWell[i] = -1;
	}
	IloIntArray active_compressors(env); //Storage the already active compressors
	double obj = 0.0;
	int unLocatedWells = inst.wells;

	int j, c, i;
	double varValue;
	priority_queue<Column,vector<Column>, CompareColumn> pQC;	
	for (j = 0; j < inst.compressors; j++){
		maxGasRate[j] = inst.qCMax[j];
		for (c = 0; c < lambda[j].getSize(); c ++){
			varValue = cplex.getValue(lambda[j][c]);
			if (varValue > NEAR_ONE){	//Add to solution the 1(integer)-variables
				obj += inst.cJ[j];				
				for (i = 0; i < inst.wells; i++){					
					if (delta[j][c][i] > NEAR_ONE){
						solutionWell[i] = j;
						gasRate[j] += inst.qW[i];
						obj += inst.cIJ[j][i];
						unLocatedWells--;
						if (inst.qMaxIJ[j][i] < maxGasRate[j]){
							maxGasRate[j] = inst.qMaxIJ[j][i];
						}
					}
				}
				operCosts[j] = inst.dJ[j] * gasRate[j] * getPressure(inst, j, gasRate[j]);
				obj += operCosts[j];
			}else if(varValue > RC_UB && varValue <= NEAR_ONE){ //Storage the fractional variables in the priority queue
				Column col{	j, c, varValue};			
				pQC.push(col);
			}
		}
	}
	//Try to put the fractional columns in the solution		
	next:
	while(pQC.size() > 0){ 
		IloIntArray supplyWells(env); //storage the wells supplied by the column
		Column col = pQC.top();	//Get the columns by decrease value of the variable			
		pQC.pop();		
		if(gasRate[col.j] <= RC_UB){ //If the compressor isn't installed yet
			for (i = 0; i < inst.wells; i++){
				if (delta[col.j][col.col][i] > NEAR_ONE){
					if (solutionWell[i] != -1) goto next;
					else{
						supplyWells.add(i);
					}
				}
			}
			//Only if the column can be totally add
			obj += inst.cJ[col.j];			
			for (i = 0 ; i < supplyWells.getSize(); i++){
				solutionWell[supplyWells[i]] = col.j;
				gasRate[col.j] += inst.qW[supplyWells[i]];
				obj +=	inst.cIJ[col.j][supplyWells[i]];
				unLocatedWells--;
				if (inst.qMaxIJ[col.j][supplyWells[i]] < maxGasRate[col.j]){
					maxGasRate[col.j] = inst.qMaxIJ[col.j][supplyWells[i]];
				}
			}
			operCosts[col.j] = inst.dJ[col.j] * gasRate[col.j] * getPressure(inst, col.j, gasRate[col.j]);
			obj += operCosts[col.j];
		}		
	}
	
	//Allocate the unsupplied wells - random select a compressor to install
	int seed = inst.wells*nodes;
	//~ cout << "Uwells " << unLocatedWells << endl;
	while (unLocatedWells > 0){		
		sort:
		seed--;
		j = iRand(0, inst.compressors-1, seed);			
		if (gasRate[j] > RC_UB){			
			goto sort;
		}		
		int canInstall = 0; //Inform if the compressor can supply some well and therefore can be installed
		for (i = 0; i < inst.wells; i++){
			if (solutionWell[i] == -1){
				if (inst.orac[j][i] == 1 && gasRate[j]+inst.qW[i] <= maxGasRate[j] && gasRate[j]+inst.qW[i] <= inst.qMaxIJ[j][i]){					
					//~ cout << "well " << i << " to comp " << j << endl;
					canInstall = 1;
					unLocatedWells--;
					solutionWell[i] = j;
					obj += inst.cIJ[j][i];
					gasRate[j] += inst.qW[i];
					if (inst.qMaxIJ[j][i] < maxGasRate[j]){
						maxGasRate[j] = inst.qMaxIJ[j][i];
					}
				}				
			}
		}
		if (canInstall == 1){
			operCosts[j] = inst.dJ[j]*gasRate[j]*getPressure(inst, j, gasRate[j]);
			obj += inst.cJ[j] + operCosts[j];
		}
	}
	double objA = obj;
	//~ cout << "Before local search - Obj " << obj << endl;
	
	///-------------------------Local search
	#ifdef LocalSearch
	#ifdef BestImprovment
	//Swap - try to swap the well i and i1 
	for (i = 0; i < inst.wells; i++){
		double best_dif = 0; //Storage the best difference value between the permutation
		int best_swap_well = -1;
		for (int i1 = i+1; i1 < inst.wells; i1++){			
			if (canSwap(inst, i, i1, solutionWell, gasRate, maxGasRate)){			
				double current_cost = operCosts[solutionWell[i]] + inst.cIJ[solutionWell[i]][i] + operCosts[solutionWell[i1]] + inst.cIJ[solutionWell[i1]][i1];					
				double new_cost = swapCost(inst, i, i1, solutionWell, gasRate, operCosts);								
				if (new_cost < current_cost){
					double dif = current_cost - new_cost;
					if (dif > best_dif){
						best_dif = dif;
						best_swap_well = i1;
					}
					//~ swap(inst, i, i1, solutionWell, gasRate, maxGasRate, operCosts, obj);					
					//~ break; //necessary when do the swap in a First Improvment approach
				}
				
			}
		}
		if (best_swap_well != -1) swap(inst, i, best_swap_well, solutionWell, gasRate, maxGasRate, operCosts, obj);
	}
	#endif

	#ifndef BestImprovment //First improvment
	for (i = 0; i < inst.wells; i++){
		for (int i1 = i+1; i1 < inst.wells; i1++){			
			if (canSwap(inst, i, i1, solutionWell, gasRate, maxGasRate)){			
				double current_cost = operCosts[solutionWell[i]] + inst.cIJ[solutionWell[i]][i] + operCosts[solutionWell[i1]] + inst.cIJ[solutionWell[i1]][i1];					
				double new_cost = swapCost(inst, i, i1, solutionWell, gasRate, operCosts);								
				if (new_cost < current_cost){
					swap(inst, i, i1, solutionWell, gasRate, maxGasRate, operCosts, obj);					
					break; //necessary when do the swap in a First Improvment approach
				}
				
			}
		}
	}
	#endif
	//~ cout << "After local search - Obj " << obj << endl;
	#endif

	//Re-calcule the cost
	//~ IloNumArray gas(env, inst.compressors);
	//~ double cost = 0;
	//~ for (i = 0; i < inst.wells; i++){
			//~ gas[solutionWell[i]] += inst.qW[i];
				//~ cost += inst.cIJ[solutionWell[i]][i];			
		//~ }
	//~ for (j = 0 ; j < inst.compressors; j++){
		//~ if (gas[j] > RC_UB){
			//~ cost += inst.cJ[j] + inst.dJ[j] * gas[j] * getPressure(inst, j , gas[j]);			
		//~ }		
	//~ }
	//~ cout << " Real cost " << cost << endl;
	return obj;
}

void Branch_Price(Timer<std::chrono::milliseconds>& timer_global, IloEnv &env, const Instance& inst, CGModel& cgModel, IloArray<pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time,const nodeBP& node, IloNum& ub, NumNumMatrix& solution) {
	if (timer_global.reachedTimeLimit()){
		timer_global.stop();
		return;
	}
	pUb = ub;
	nodes++;
	cgModel.cplex.setParam(IloCplex::TiLim, timeL-timer_global.total()/1000);
	if (nodes == 1){
		 for (int j = 0; j < inst.compressors; j++) n_cols += cgModel.lambda[j].getSize();		 
	}
	//Variables to guide the branching rules
	IloNumArray sum_colJ(env, inst.compressors); //Storage the sum on variables y of the relaxed model
	NumNumMatrix sum_colIJ(env, inst.compressors); //Storage the sum on variables x of the relaxed model
	IloNum varValue;
	int compressor = -1, well=-1;
	
	//~ cout << "Node " << nodes << "\t UB " << ub;
	IloNum lb = 0;
	unsigned int typeNode = 2; //default type 2
	
	//Build a random array for the LAHC
	IloIntArray seeds(env, 100);
	for (int ds=0; ds < seeds.getSize(); ds++){
		seeds[ds]=iRand(0,999,ds);		
	}			
	
	//~ if(cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node, ub, cgIt, cplexNodes, early_branch, timer_global, timeL, seeds, nodes, 0, 0, 0)){ //for compile iRace CG
	if(cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node, ub, cgIt, cplexNodes, early_branch, timer_global, timeL, seeds, nodes)){
		if (nodes == 1) time_root = mp_time + p_time;		
		int cg_status = cgModel.getStatus(inst.compressors);
		if (cg_status == 3){ //Infeasible (using slack variables)			
			cuts++;			
			//~ cout << " inf(artificial) " << endl;
			return;
		}
		lb = cgModel.cplex.getObjValue();
				
		if (cg_status == 2){ //Integral
			cuts++;		
			//New best upper bound
			if(lb < ub){
				ub = lb;				
				#ifndef NStorageSolution
				///Clear the previous result and Storage the result - TODO improve the form to get the result - pegar do IloNumVarArray
				for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= NEAR_ONE){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
					}
				}
				#endif
			}
			//~ cout << " integer " << endl;
			return;
		}				
		if (lb >= ub){			
			cuts++;
			//~ cout << " LB > UB" << endl;
			return;
		}

///Storage the relaxed RMP model variables values 		
		cgModel.relaxModelValues = NumNumMatrix(env, inst.compressors);
		int jMin1=-1, jMax1=-1;	 //type I
		int jMin2=-1, jMax2=-1, iMin2=-1, iMax2=-1, colMin2=-1, colMax2=-1;	//type II
		double maxV1 = RC_UB, minV1 = NEAR_ONE;
		for (int j = 0; j < inst.compressors; j++){
			cgModel.relaxModelValues[j] = IloNumArray(env, cgModel.lambda[j].getSize());
			cgModel.cplex.getValues(cgModel.lambda[j], cgModel.relaxModelValues[j]);
			sum_colJ[j] = IloSum(cgModel.relaxModelValues[j]);
			if (sum_colJ[j] > RC_UB && sum_colJ[j] < NEAR_ONE){
				typeNode = 1;
				if (sum_colJ[j] >= maxV1){ //Closer to 1
					maxV1 = sum_colJ[j];
					jMax1 = j;
				}else if (sum_colJ[j] <= minV1){//Closer to 0
					minV1 = sum_colJ[j];
					jMin1 = j;
				}				
			}
			if (typeNode == 1) goto jumpNodeII; // Se em algum ponto ver que pode fazer branch do tipo I, ignora o tipo II

			//Info for node type 2
			sum_colIJ[j] = IloNumArray(env, inst.wells);
			for(int c = 0; c < cgModel.lambda[j].getSize(); c++){												
				for (int i = 0 ; i < inst.wells; i++){
					if (cgModel.delta[j][c][i] > NEAR_ONE){
						sum_colIJ[j][i] += cgModel.cplex.getValue(cgModel.lambda[j][c]);
					}
				}
			}
			jumpNodeII:{}
		}
		//Define between jMin and jMax the most close to integrality - Node type I
		if (1.0 - (floor(minV1 * DecPrecision) / DecPrecision) > (floor(maxV1 * DecPrecision) / DecPrecision)){			
			varValue = minV1;
			compressor = jMin1;	
		}else{
			varValue = maxV1;
			compressor = jMax1;		
		}		

///Primal heuristic
#ifndef RMPZ //Define a feasible solution		
		//~ double newUb = cgModel.primalHeuristic(inst, env);
		//~ double newUb = cgModel.greedyWSC(inst,env);
		//~ cgModel.setPartitioningMaster(inst, env, nodes);
		timer_ms.start();
		NumNumMatrix s(env,inst.compressors);
		double newUb = cgModel.heuristicRMPB(inst,env, nodes, s);
		mp_time += timer_ms.total();		
		if (newUb != -1){
			//~ cout << "Inst = " << ins << " Found an upper bound " << newUb << " node " << nodes << endl;
			if (newUb < ub && (ub-newUb) > RC_UB){
				 ub = newUb;
				 #ifndef NStorageSolution
				 for (int j = 0; j < inst.compressors; j++){ 
					solution[j] = IloNumArray(env, inst.wells);
					solution[j] = s[j];
				 }
				 #endif
				 //~ cout << "Greedy value = " << newUb << " updated UB";
			}
			cout << endl;
		}			 
#endif			
///Solve the RMP in integer form		
#ifdef RMPZ	
//~ if (1 == 1){ //All nodes are solved
//~ if (solveAtEachNodes < inst.compressors){ // Only if no improvment happend in the last n nodes
	//~ cout << " solveAtEachNodes " << solveAtEachNodes << " lastNodeUpdate " << lastNodeUpdate;
	if (nodes - lastNodeUpdate >= solveAtEachNodes){ //If the node should be solved
		lastNodeUpdate = nodes;
		//Convert the lambda variables to INT and remove all constraints 
		IloArray<IloConversion> conv(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){			
			conv[j] = IloConversion(env, cgModel.lambda[j], ILOINT);
			cgModel.model.add(conv[j]);
			///Remove constraints of B&P
			cgModel.FacilityAssignment[j].setBounds(0, 1);
			for (int i =  0; i < inst.wells; i++){
				cgModel.BranchConst[j][i].setBounds(0,1);
			}
			///
		}		
		timer_ms.start();		
		if(cgModel.cplex.solve()){
			cplexNodes += cgModel.cplex.getNnodes();		
			mp_time += timer_ms.total();
			IloNum obj = cgModel.cplex.getObjValue();			
			if (obj < ub && (ub-obj) > RC_UB){
				//~ cout << " Found a new upper bound";
				solveAtEachNodes = max(1.0, round(solveAtEachNodes/coefNodeRmp));
				ub = obj;
				///Clear the previous result and Storage the result
				#ifndef NStorageSolution
				for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
					}
				}
				#endif
				//~ cout << " Solution obj Integer " << ub << endl;
				double value = 0;				
			}else{
				//~ cout << " No improvment ";
				solveAtEachNodes = max(1.0, round(solveAtEachNodes*coefNodeRmp));
			}
			//~ cout  << endl;						
		}else{
			//~ cout << " No feasible solution " << endl;				
			solveAtEachNodes = max(1.0, round(solveAtEachNodes*coefNodeRmp));
			mp_time += timer_ms.total();
		}
	
		for (int j = 0; j < inst.compressors; j++){  
			cgModel.model.remove(conv[j]);//Remove the conversion
			if (branchRuleL1[j] == 1){ // Re-add the constraints
				cgModel.FacilityAssignment[j].setLB(1);
			}else if (branchRuleL1[j] == 2){
				cgModel.FacilityAssignment[j].setUB(0);
			}
			for (int i = 0; i < inst.wells; i++){
				if(branchRuleL2[j][i] == 1){
					cgModel.BranchConst[j][i].setLB(1);
				}else if(branchRuleL2[j][i] == 2){
					cgModel.BranchConst[j][i].setUB(0);
				}				
			}
		}
	}
	//~ else cout << endl;
#endif
///~		
	}else{//Infeasible 		
		cuts++;
		//~ cout << " inf " <<endl;		
		return;
	}
///Fractional soltion
//Identify the level type of the next node and find the y (1st) or lambda(2st) variable that is closer to integrality	
	
//1st level
//~ cout << "Type Node " << typeNode << endl;
	if (typeNode == 1){
		//cgModel.findCloseIntegralityLevel1(env, inst, varValue, compressor, sum_colJ); //TODO IDENTIFY BEFORE (NO USIGN THE METHOD)
		goto createNode;		
	}
//2nd level
	cgModel.findCloseIntegralityLevel2(env, inst, varValue, compressor, well, sum_colIJ);	
	
	createNode:
	IloIntArray b(env, 2); 
//Define the order of the RHS 
	if (varValue < 0.5){ 
		b[0] = 0;
		b[1] = 1;
	}else{
		b[0] = 1;
		b[1] = 0;
	}

	for (int i = 0; i < b.getSize(); i++){
		///Create the node 
		nodeBP nodeB(env, compressor, well, b[i], typeNode);
		nodeB.lb = lb;
//Copy the UB of the variables x of the previous node - its need only for 2nd level
		nodeB.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			nodeB.ubPricing[j] = IloIntArray(env, inst.wells);
			for (int id = 0; id < inst.wells; id++){
				nodeB.ubPricing[j][id] = node.ubPricing[j][id];
			}
		}
///Verify Node Type			
		if (nodeB.type == 1){
			if (b[i] == 1){ 
				cgModel.FacilityAssignment[nodeB.j].setLB(1);
				branchRuleL1[nodeB.j] = 1;
			}else{ //b[i] == 0				
				cgModel.FacilityAssignment[nodeB.j].setUB(0);
				branchRuleL1[nodeB.j] = 2;
			}
		}else{//Type = 2			
			//Insert the pricing constraints and storage the modifications in the x variables in this node.
			for (int j = 0 ; j < inst.compressors; j++){
				if(b[i] == 0){
					if(j == nodeB.j){
						cgModel.BranchConst[j][nodeB.i].setUB(0); //RMP
						subproblem[j].x[nodeB.i].setUB(0); 		//Pricing
						nodeB.ubPricing[j][nodeB.i] = 0;
						branchRuleL2[j][nodeB.i] = 2;
					}
				}else{ //b[i]==1
					if(j == nodeB.j){
						cgModel.BranchConst[j][nodeB.i].setLB(1); //RMP
						subproblem[j].x[nodeB.i].setLB(1); //Pricing
						branchRuleL2[j][nodeB.i] = 1;
					}else if(node.ubPricing[j][nodeB.i] == 1){ //The ub is set to 0 and stored in ubPricing only if in the previos x.ub was 1
						subproblem[j].x[nodeB.i].setUB(0); //Pricing
						nodeB.ubPricing[j][nodeB.i] = 0;   //    
					}
				}
			}

		}
		///Branch and price recursive
		depth++;
		if (depth > max_depth) max_depth = depth;
		Branch_Price(timer_global, env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, nodeB, ub, solution);		
		depth--;	

		//Remove restrictions 
		if(nodeB.type == 1){
			branchRuleL1[nodeB.j] = 0;
			if (b[i] == 1){
				cgModel.FacilityAssignment[nodeB.j].setLB(0);			
			}else{ //b[i]=0
				cgModel.FacilityAssignment[nodeB.j].setUB(1);			
			}
		}else{///Type == 2
			//Remove restrictions of the pricing in j
			if (b[i] == 0){
				cgModel.BranchConst[nodeB.j][nodeB.i].setUB(1);				
				subproblem[nodeB.j].x[nodeB.i].setUB(1);
				nodeB.ubPricing[nodeB.j][nodeB.i] = 1;
			}else{//b[i]=1
				cgModel.BranchConst[nodeB.j][nodeB.i].setLB(0);
				subproblem[nodeB.j].x[nodeB.i].setLB(0);			
			}
			branchRuleL2[nodeB.j][nodeB.i] = 0;
			for (int comp = 0; comp < inst.compressors; comp++){
				//Remove restrictions of the pricing in N\j 
				if (b[i] == 1){
					if(comp != nodeB.j && node.ubPricing[comp][nodeB.i] >= NEAR_ONE){
						subproblem[comp].x[nodeB.i].setUB(1);
						nodeB.ubPricing[comp][nodeB.i] = 1;					
					}
				}
			}
		}
	}
}

void csp::bPrice(string file, const int n_ini, const int n_fim, const double& timeLimit, const double& coefRmp){
	stringstream out;
	out << 4 << "_Inst_" << n_ini << "_to_" << n_fim << "_" << timeLimit << ".txt";
	ofstream output(out.str());
	output << fixed;
	output.precision(3);
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}

	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "SPj\t" << "Root\t" << "Total\t" << "Obj\t Obj(p)\t"  <<  "Nodes\t CpxNds\t Cuts\t CGIter\t #ColsR\t #ColsF\t MaxDepth\t Early_branch\t" << "Solution" << endl;
	
	for (ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global(timeLimit * 1000);
		timeL = timeLimit;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime {0};
		time_root= 0;
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
				
		IloNum ub = {numeric_limits<double>::max()};			
		pUb = 0;
		cuts = 0;
		nodes = 0;
		
		lastNodeUpdate = 0;
		coefNodeRmp = coefRmp;
		solveAtEachNodes = 1;
		
		cgIt = 0;
		depth = 1;
		max_depth = 1;
		n_cols = 0;
		n_colsF = 0;
		cplexNodes = 0;
		early_branch = 0;
		
		NumNumMatrix solution(env,inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			solution[j] = IloNumArray(env, inst.wells);
		}
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);

			nodeBP node(env,0,0,0,1);
			node.lb = 0;
			node.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
			//Storage the current branch rules in the B&P (0 = Default; 1 = Must be appear; 2 = Forbid in the node)
			branchRuleL1 = IloIntArray(env, inst.compressors);
			branchRuleL2 = IloArray<IloIntArray>(env, inst.compressors);

			for (int i = 0; i < inst.compressors; i++){
				node.ubPricing[i] = IloIntArray(env, inst.wells);
				branchRuleL2[i] = IloIntArray(env, inst.wells);
				for (int j = 0; j < inst.wells; j++){
					node.ubPricing[i][j] = 1;
				}
			 }
						
			//BEGIN OF RECURSION
			Branch_Price(timer_global,env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, node, ub, solution);
			totalTime += timer_global.total();
			for (int j = 0; j < inst.compressors; j++) n_colsF += cgModel.lambda[j].getSize();
			
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << time_root/1000 << "\t" << totalTime/1000 << "\t" << ub << "\t" << pUb << "\t" << nodes <<
			"\t" << cplexNodes << "\t" << cuts << "\t" << cgIt << "\t" << n_cols << "\t" << n_colsF << "\t" << max_depth << "\t" << early_branch << "\t";
			
			#ifndef NStorageSolution
			double final_cost = 0;
			IloNumArray gasR(env, inst.compressors);
			
			for (int j = 0; j < inst.compressors; j++){
				output << j << "{";				
				for (int i = 0; i < inst.wells; i++){					
					if (solution[j][i] >= NEAR_ONE){
						 if (inst.orac[j][i] == 0) cout << j << " " << i << "ERRADO \n";
						 output << i << ",";						 
						 final_cost += inst.cIJ[j][i];
						 gasR[j] += inst.qW[i];
					}
				}				
				output << "} ";
			}
			for (int j = 0; j < inst.compressors; j++){
				if (gasR[j] > 0){
					final_cost += inst.cJ[j] + inst.dJ[j] * gasR[j] * cgModel.getPressure(inst, j, gasR[j]);
					if (gasR[j] > inst.qCMax[j]) cout << "Maior que qCMax[" << j << "]" << endl;
					for (int i = 0; i < inst.wells; i++){
						if (solution[j][i] >= NEAR_ONE && gasR[j] > inst.qMaxIJ[j][i]) cout << "Maior que qMaxIJ[" << j << "][" << i << "]" << endl;
					}
				}
			}
			
			output << " Final cost (aprox.) = " << final_cost; 
			#endif
			output << endl;
			//Print Columns
			//~ for (int j = 0; j < inst.compressors; j++){
				//~ for (int c = 0; c < cgModel.lambda[j].getSize(); c++){
					//~ cout << j <<  " " << c ;
					//~ for (int i = 0 ; i < inst.wells; i++){
						//~ cout << " " << cgModel.delta[j][c][i];
					//~ }
					//~ cout  << " " << " qJ: " << cgModel.qjS[j][c] << endl;
				//~ }
			//~ }
			//~ cout << cgModel.cplex.getObjValue() << endl;
			
		}catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
