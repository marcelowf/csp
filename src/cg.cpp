// Date: March/2015
// Description: Column Generation for Compressor Scheduling Problem
// Based on: "Column Generation for Solvin a Compressor Scheduling Problem" and "A revised Model for compressor design and scheduling in gas-lifted oil fields
// Eduardo Camponogara and Augustinho Plucenio
// Version with recalc of reduced cost - provide an exact objective value.
// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"

#define RC_LB -1.0e-6
#define RC_UB 1.0e-6
#define NEAR_ONE 0.999999
#define DELIMITER "_"

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using csp::Instance;
using csp::CGModel;
using csp::pricing;

double CGModel::getPressure(const Instance& inst, const int& j, const double& qj){
	double gasRate;
	if (qj <= 0){
		gasRate = inst.qCMin[j];		
	}else {
		gasRate = qj;
	}
	return inst.alpha0[j] + inst.alpha1[j]*gasRate + inst.alpha2[j]*(pow(gasRate,2.0)) + 
	inst.alpha3[j]*(pow(gasRate,3.0)) + inst.alpha4[j]*(log(1 + gasRate));	
}

double CGModel::costColum(const IloInt& wells, const IloNumArray& delta, const IloInt& compressor, const IloNumArray& cJ, 
				 const NumNumMatrix& cIJ, const IloNumArray& dJ, const IloNum& qC, const IloNum& pC){
	  IloNum cost = cJ[compressor];
	  cost += IloScalProd(cIJ[compressor], delta);
	  cost += dJ[compressor]*qC*pC;
	  return cost;
}
  		
void CGModel::init(IloEnv& env, const int& compressors, const int& wells){
    env.setNormalizer(IloFalse);
	cplex.setOut(env.getNullStream()); //Não mostrar iterações na tela.
	//~ cplex.setDeleteMode(IloCplex::FixBasis);
				
    lambda = NumVarMatrix(env,compressors); //Array de Array de variáveis [n]x[#col]      
    delta = IloArray<NumNumMatrix>(env, compressors);
    qjS = NumNumMatrix(env, compressors);
    cjS = NumNumMatrix(env, compressors);
    
    ClientServiced = IloRangeArray(env, wells, 1, 1);
    FacilityAssignment = IloRangeArray(env, compressors, -IloInfinity, 1); 
    
    model.add(ClientServiced);
    model.add(FacilityAssignment);  
}

void pricing::init(IloEnv& env, const Instance& inst, const IloInt& j){
	//cplex.setParam(IloCplex::AdvInd, 0);
	cplex.setOut(env.getNullStream());
	//~ cplex.setParam(IloCplex::RandomSeed, 123456789);
	int i, ka;
	int n = inst.compressors;
	int m = inst.wells;
	int k = inst.k;
	//Variables
	x = IloNumVarArray(env, m, 0, 1, ILOINT);
	y = IloNumVarArray(env, m, 0, 1, ILOINT); //Extra
	q = IloNumVar(env);//, 0, IloInfinity,ILOFLOAT);
	z = IloNumVarArray(env, k, 0, 1, ILOINT);
	lambdaL = IloNumVarArray(env, k, 0, 1, ILOFLOAT);
	lambdaR = IloNumVarArray(env, k, 0, 1, ILOFLOAT);

	
	//Objective
	IloExpr expr(env);
	expr.clear();
	for (i = 0; i < m; i++){
		if(inst.orac[j][i] == 1) expr += x[i]; 
	}
	for (ka = 1; ka < k; ka++){
		expr += (inst.pointsH[j][ka-1]*lambdaL[ka] + inst.pointsH[j][ka]*lambdaR[ka]);
	}
	SPj.setExpr(expr);
	model.add(SPj);
	
	//Constraints
	//3b
	expr.clear();
	for (ka = 1; ka < k; ka++){
		expr += (inst.pointsQ[j][ka-1]*lambdaL[ka] + inst.pointsQ[j][ka]*lambdaR[ka]);
	}
	model.add(q == expr);
	
	//3c - modified with new variable y1 (20-02-15)
	for(i = 0; i < m; i++){
		if (inst.orac[j][i] == 1){ 
			model.add(q <= inst.qMaxIJ[j][i]*x[i] + inst.qCMax[j]*y[i]);		
			model.add(x[i]+y[i] == 1);
		}
		//~ if (inst.orac[j][i] == 1) model.add(q <= inst.qMaxIJ[j][i]*x[i] + inst.qCMax[j]*(1.0 - x[i]));				
	}

	//3d
	expr.clear();	
	//~ for(i = 0; i < m; i++){
		//~ if (inst.orac[j][i] == 1) expr+= inst.qW[i]*x[i];
	//~ }
	expr = IloScalProd(inst.qW,x);
	model.add(expr <= q);
	//~ model.add(expr == q);  //Original constraint of the problem in case 2008
	
	//3e
	model.add(IloSum(z) == 1);
	
	//3f
	for (ka = 1; ka < k; ka++){
		model.add(lambdaL[ka]+lambdaR[ka] == z[ka]);
	}
	
	//~ for (i = 0; i < m; i++){
		//~ //3i & extra - restringir a 0 as variaveis x que não fazem parte de MJ
		//~ if (inst.orac[j][i] == 0) model.add(x[i] == IloFalse);		
	//~ }
}
void pricing::setDualCoef(const IloInt& j, const IloNum& miD, const IloNumArray& piD, const Instance& inst){
	for (int i = 0; i < inst.wells ; i++){
		if (inst.orac[j][i] == 1){
			SPj.setLinearCoef(x[i], inst.cIJ[j][i]-piD[i]);
		}
	}
	SPj.setConstant(inst.cJ[j] - miD);
}

void CGModel::greedyIntialColumns(IloEnv& env, const Instance& inst){
	int i, j, c;
	for(j = 0; j < inst.compressors; j++){
		delta[j] = NumNumMatrix(env);
		delta[j].add(IloNumArray(env, inst.wells));
				
		cjS[j] = IloNumArray(env);
		cjS[j].add(0);
		
		qjS[j] = IloNumArray(env);
		qjS[j].add(0);
		
		lambda[j] = IloNumVarArray(env);
		IloNum qmin = 1000.0;
		c = delta[j].getSize()-1;
		
		for (i = 0; i < inst.wells; i++){			
			if (inst.orac[j][i] == 1 && (qjS[j][c]+inst.qW[i]) <= inst.qMaxIJ[j][i] && (qjS[j][c]+inst.qW[i]) <= qmin){
				delta[j][c][i] = 1;
				qjS[j][c] += inst.qW[i];
				if (inst.qMaxIJ[j][i] < qmin){
					qmin = inst.qMaxIJ[j][i];
				}				
			}else if (inst.orac[j][i] == 1){
				delta[j].add(IloNumArray(env, inst.wells));
				qjS[j].add(inst.qW[i]);
				cjS[j].add(0);
				
				c = delta[j].getSize()-1;
				delta[j][c][i] = 1;				
				qmin = inst.qW[i];				
			}
		}
		//Slack variables (to ensure initial feasible solution for RMP)
		delta[j].add(IloNumArray(env, inst.wells));
		cjS[j].add(100000);
		c = delta[j].getSize()-1; //'c' is the last index of the column (in the case, the slakc)
		for(i = 0; i < inst.wells; i++){
			delta[j][c][i] = 1;
		}
		
		//Add var to the array lambda 
		for (int col = 0; col < c; col++){//Greedy columns
			//Create a name 'type*j_col'
			string s = "1*" + to_string(j) + "_" + to_string(col);
			char const* name = s.c_str();
			
			cjS[j][col] = costColum(inst.wells, delta[j][col], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][col], getPressure(inst, j, qjS[j][col]));
			
			lambda[j].add( cost(cjS[j][col]) +  ClientServiced(delta[j][col]) + FacilityAssignment[j](1) );
			lambda[j][col].setName(name);
		}		
		//Slack
		cjS[j][c] = pow(IloSum(inst.cJ),2);
		lambda[j].add( cost(cjS[j][c]) +  ClientServiced(delta[j][c]) + FacilityAssignment[j](1));
		
		string s = "0*" + to_string(j) + "_" + to_string(c);
		char const* name = s.c_str();
						
		lambda[j][c].setName(name);
		
		//Add to model - no make difference
		//~ model.add(lambda[j]);
	}
}

void CGModel::addColumn(IloEnv env, const IloInt& j, const IloNum& columnCost, const IloNumArray& coef, const Instance& inst){
	delta[j].add(IloNumArray(env, inst.wells));
	for(int i = 0; i < inst.wells; i++){
		delta[j][delta[j].getSize()-1][i] = coef[i];
	}
	cjS[j].add(columnCost);
	
	//Name construct
	string s =  "1*" + to_string(j) + "_" + to_string(cjS[j].getSize()-1);
	char const* name = s.c_str();
	
	//Objective and constraint 5b/5c		
	lambda[j].add(cost(columnCost) + ClientServiced(coef) + FacilityAssignment[j](1) );
	
	//Add name	
	lambda[j][lambda[j].getSize()-1].setName(name);
}

int CGModel::getStatus(const IloNum& n){
	//* 1 if is fractional
	//* 2 if is integral.
	//* 3 if is infeasible (uses artificial variables)
	int j, c, result;
	result = 2;
	for(j = 0; j < n; j++){
		for(c = 0; c < lambda[j].getSize(); c++){
			string varName = lambda[j][c].getName();
			int varType = atoi(varName.substr(0, 1).c_str());
			if (varType == 0){ 
				if(cplex.getValue(lambda[j][c]) > RC_UB){
					result = 3;
					goto ex;
				}
			}
			if (cplex.getValue(lambda[j][c]) > RC_UB && cplex.getValue(lambda[j][c]) < NEAR_ONE){
				result = 1;				
			}
		}		
	}
	ex:	return result;
}

bool CGModel::cg_iter(IloEnv& env, const Instance& inst, IloArray<csp::pricing>& subproblem, Timer<chrono::milliseconds>& timer_ms, float& mp_time, float& p_time){
	timer_ms.start();
	if(!cplex.solve()){ //This never happen in the first iteration of B&P because the slack variables
		mp_time += timer_ms.total();		
		return false;
	}	
	mp_time += timer_ms.total();
	
	int cont; //Controla o nº de colunas adicionadas ao master
	NumNumMatrix coef(env, inst.compressors); 
	
	int i,j, ka;
	int n = inst.compressors;
	int m = inst.wells;
	int k = inst.k;
			
	IloNumArray pi(env, inst.wells);
	IloNumArray mi(env, inst.compressors);		
	for(;;){
		//Get dual variables
		cplex.getDuals(pi, ClientServiced); //5b
		cplex.getDuals(mi, FacilityAssignment); //5c		
		double pricing_redCost = 0;
		cont = n; 
		for(j = 0; j < n; j++){
			coef[j] = IloNumArray(env, inst.wells);
			subproblem[j].setDualCoef(j, mi[j], pi, inst);			
			//Execute pricing
			timer_ms.start();
			if(subproblem[j].cplex.solve()){	
				p_time +=  timer_ms.total();
				//Storage objectiveValue 
				pricing_redCost = subproblem[j].cplex.getObjValue();			
				
				if (pricing_redCost < RC_LB){
					///Recalculate reducedCost
					subproblem[j].cplex.getValues(coef[j], subproblem[j].x);					
					double qJ = subproblem[j].cplex.getValue(subproblem[j].q);
					double pJ = getPressure(inst, j, qJ);
					double cCost = costColum(inst.wells, coef[j], j, inst.cJ, inst.cIJ, inst.dJ, qJ, pJ);
					double redCost = cCost-mi[j] - IloScalProd(coef[j], pi);
					
					if (redCost < RC_LB){
						///To use the aproximated value of the column 
						double columnCostP = pricing_redCost + mi[j] + IloScalProd(coef[j], pi);
						addColumn(env, j, columnCostP, coef[j], inst); //Coluna do Pricing
						//~ for (int i = 0; i < inst.wells; i++){
							//~ if (inst.orac[j][i] == 0 && coef[j][i] > 0) cout << "Coluna infactível adicionada" << endl;
						//~ }
						///
						
						///Column with exact value of p(q) 
						//~ addColumn(env, j, cCost, coef[j], inst); //Coluna recalculada
						///
						cont--;
					}
				}
			}//else  {cout << "INFEASIBLE PRICING" << endl;}
		}
		if (cont == n){ //Caso nenhuma coluna seja adicionada, i.e. todos SPj são >=0
			break;			
		}else{
			timer_ms.start();
			cplex.solve();	//Never will be infeasible		
			mp_time +=  timer_ms.total();				
			//~ cplex.exportModel("master.lp");
		}
	}
	return true;
}

void csp::cg(string file, const int n_ini, const int n_fim) {
	stringstream out;
	out << 3 << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	
	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}
		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime{0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
						
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);
						
			//Solve root node			
			cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time);		
			
			totalTime += timer_global.total();
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << cgModel.cplex.getObjValue() << "\t";
			
			int cg_status = cgModel.getStatus(inst.compressors);
			if (cg_status == 1) output << "Feasible" << endl;
			else if (cg_status == 2) output << "Optimal" << endl;
			else output << "Infeasible" << endl;
			
		}
		catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
