// -------------------------------------------------------------- -*- C++ -*-
// Autor: Marcelo Friske
// Date: September/2014
// Description: Algorithm for Compressor Scheduling Problem, picewise linear formulation
// with binary formulations. Based on: "A revised model for compressor design and scheduling
// in gas-lifted oil fields". E. Camponogara, L. Nazari and C. Meneses (2011).
// PS: Com adição de restrições para definir os subconjuntos Ni e Mj
// 
// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using csp::Instance;

void Instance::readFile(IloEnv& env, ifstream& input){
	cJ = IloNumArray(env); 
	dJ = IloNumArray(env); 
	qCMin = IloNumArray(env); 
	qCMax = IloNumArray(env); 
	alpha0 = IloNumArray(env); 
	alpha1 = IloNumArray(env); 
	alpha2 = IloNumArray(env); 
	alpha3 = IloNumArray(env); 
	alpha4 = IloNumArray(env); 
	pointsQ = NumNumMatrix(env); 
	pointsH = NumNumMatrix(env);

	qW = IloNumArray(env); 
	pW = IloNumArray(env); 

	orac = NumNumMatrix(env); 
	cIJ = NumNumMatrix(env); 
	lIJ = NumNumMatrix(env);  
	qMaxIJ = NumNumMatrix(env); 

	string s;
	IloNum c, d;
	IloInt temp, x;
	
	//Parameters
	input >> compressors >> wells >> k;	
	
	/*****Scan file*****/
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}	
	//Compressor data	
	for (int i = 0; i < compressors; i++){
		pointsQ.add(IloNumArray(env, k));
		pointsH.add(IloNumArray(env, k));		
		
		cIJ.add(IloNumArray(env, wells));		
		lIJ.add(IloNumArray(env, wells));		
		qMaxIJ.add(IloNumArray(env, wells));
		orac.add(IloNumArray(env, wells));	
		
		double compCost, energyCost, rateMin, rateMax, a0, a1, a2, a3, a4;
		input >> temp >> compCost >> energyCost >> rateMin >> rateMax >> a0 >> a1 >> a2 >> a3 >> a4;
		cJ.add(compCost);
		dJ.add(energyCost);
		qCMin.add(rateMin);
		qCMax.add(rateMax);
		alpha0.add(a0);
		alpha1.add(a1);
		alpha2.add(a2);
		alpha3.add(a3);
		alpha4.add(a4);
		//~ output << " " << temp << "  " << cJ[i] << "\t" << dJ[i] << "\t" << qCMin[i] << "\t" << qCMax[i] << "\t\t"  << alpha0[i] << "\t\t" << alpha1[i] << "\t\t" << alpha2[i] << "\t\t" << alpha3[i] << "\t\t" << alpha4[i] << endl;
		
	}
	//Well data
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}
	for (int i = 0; i < wells; i++){		
		double gasCli, pCli;
		input >> temp >> gasCli >> pCli; //percorre até pw
		qW.add(gasCli);
		pW.add(pCli);
		
		//~ output << i+1 << " " << qW[i] << "\t\t" << pW[i] << "\t\t";
		
		getline(input, s); //pega o Ni
		stringstream ss(s); //Define a string como stream
		while(ss >> c){   //Take the doubles
			orac[c-1][i] = 1;		
			//~ output << c << " ";				
		}
		//~ output << endl;
	}
	//Pipeline
	for (int i = 0; i < 2; i++){
		getline(input, s);
	}	
	//~ output << "lIJ" << endl;
	while(getline(input, s)){
		if(s == "Service cost")break;
		stringstream(s) >> temp >> x >> c;
		lIJ[x-1][temp-1] = c;	
		//~ output << temp << " " << x << " " << lIJ[x-1][temp-1] << endl;	
	}
	
	//Service cost
	//~ output << "cIJ" << endl;
	getline(input, s);
	while(getline(input, s)){
		if(s == "Points")break;
		stringstream(s) >> temp >> x >> c;
		cIJ[x-1][temp-1] = c; 		
		//~ output << temp << " " << x << " " << cIJ[x-1][temp-1] << endl;			
	}
	//Points
	//~ output << "Points" << endl;
	getline(input, s);
	while(getline(input, s)){
		if(s == "Max output gas-rate")break;
		double p;
		stringstream(s) >> temp >> x >> c >> p;
		pointsQ[temp-1][x] = c; 
		pointsH[temp-1][x] = dJ[temp-1]*c*p;				
		//~ output << temp << " " << x << " " << pointsQ[temp-1][x] << "\t\t" << p << endl;
	}	
	//Max output gas-rate
	getline(input, s);
	//~ output << "Max output gas-rate" << endl;
	while(getline(input, s)){
		stringstream(s) >> temp >> x >> c;
		qMaxIJ[x-1][temp-1] = c;		
		//~ output << temp << " " << x << " " << qMaxIJ[x-1][temp-1] << endl;
	}	
}

IloNum getPressure(const IloNum& qj,const IloNum& a0, const IloNum& a1, const IloNum& a2, const IloNum& a3, const IloNum& a4){
	return a0 + a1*qj + a2*(pow(qj,2)) + a3*(pow(qj,3)) + a4*(IloLog(1 + qj));	
}
	
void csp::milp(string file, const int n_ini, const int n_fim){	
	stringstream out;
	out << "1_" << "Inst" << "_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	output << "I\t" << "N\t" << "M\t" << "Cplex\t" << "Total\t" << "FO" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float cplex_time {0};
	
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
		
	   try {
		  IloModel model(env); 
		  int i, j;
		  int n = inst.compressors;
		  int m = inst.wells;
		  int k = inst.k;
		  
		  /**** Variables ****/
		  IloNumVarArray y(env, n, 0, 1, ILOINT); // yi -> facilities {0,1}
		  y.setNames("y");
		  NumVarMatrix x(env, n); 				  // xij -> facility i assign to cliente j {0,1}
		  for (i = 0; i < n; i++){
			x[i] = IloNumVarArray (env, m, 0, 1, ILOFLOAT);			
			x[i].setNames("x");
		  }
		  IloNumVarArray q(env, n, 0, IloInfinity, ILOFLOAT); // qj -> gas-rate facility j
		  q.setNames("q");
		  NumVarMatrix lambdaL(env, n);
		  NumVarMatrix lambdaR(env, n);
		  NumVarMatrix z(env, n);
			
		  for (int j = 0; j < n; j++){
			lambdaL[j] = IloNumVarArray (env, k, 0, IloInfinity, ILOFLOAT);			
			lambdaR[j] = IloNumVarArray (env, k, 0, IloInfinity, ILOFLOAT);
			z[j] = 		 IloNumVarArray (env, k, 0 , 1, ILOINT);		
			lambdaL[j].setNames("lL");				
			lambdaR[j].setNames("lR");			
			z[j].setNames("z");					
		  }
		  /***/
		  
		  IloExpr expr(env); 
		  /** Objective function ***/
		  expr.clear();
		  for(j = 0; j < n; j++){
			  expr += inst.cJ[j]*y[j]; 
		  }
		  for (j = 0; j < n; j++){
			for (i = 0; i < m; i++){
				if(inst.orac[j][i] == 1) expr += inst.cIJ[j][i]*x[j][i]; 
			}
		  }
		  for (j = 0; j < n; j++){
			  for (i = 1; i < k; i++){
				expr += (inst.pointsH[j][i-1]*lambdaL[j][i] + inst.pointsH[j][i]*lambdaR[j][i]); 
			  }         
		  }
		  model.add(IloMinimize(env, expr));
		  
		  /***Constraints***/
		  //1b
		  for (i = 0; i < m; i++){
			for (j = 0; j < n; j++){
				if(inst.orac[j][i] == 1)	
				model.add(x[j][i] <= y[j]);
			}		
		  }
		  //1c
		  for(i = 0; i < m; i++){
			  expr.clear();
			  for (j = 0; j < n ; j++){
				expr += x[j][i];
			  }
			model.add(expr == 1); 
		  }
		  //1d
		  for(j = 0; j < n; j++){
			for(i = 0; i < m; i++){
				if(inst.orac[j][i] == 1)	model.add(q[j] <= inst.qMaxIJ[j][i]*x[j][i] + inst.qCMax[j]*(y[j] - x[j][i]));
				//~ if (inst.orac[i][j] == 1){ 
					//~ model.add(q[j] <= inst.qMaxIJ[i][j]*x[i][j] + inst.qCMax[j]*y1[j][i]);		
					//~ model.add(x[i][j]+y1[j][i] == 1);
					//~ model.add(y1[j][i] == y[j]);
				//~ }
			}
		  }
		  //6c
		  for(j = 0; j < n; j++){
			expr.clear();
			for(i = 1; i < k; i++){
				expr += (inst.pointsQ[j][i-1]*lambdaL[j][i] + inst.pointsQ[j][i]*lambdaR[j][i]);
			}  
			model.add(q[j] == expr);
		  }
		  //6d
			for(j = 0; j < n; j++){
			  expr.clear();
			  //~ model.add(IloSum(z[j]) == y[j]);
			  for(int kp = 1; kp < k; kp++){
				expr += z[j][kp];
			  }
			  model.add(expr == y[j]);		
		  }
		  //6e and 6f
		  for(j = 0; j < n; j++){
			for(i = 0; i < k; i++){
				model.add(lambdaL[j][i] + lambdaR[j][i] == z[j][i]);
				//Constraints 6f
				//~ model.add(lambdaL[j][i] >= 0);
				//~ model.add(lambdaR[j][i] >= 0);
			}
		  }
		  //1f
		  for(int j = 0; j < n; j++){
			  expr.clear();
			  for (i = 0; i < m; i++){
				if(inst.orac[j][i] == 1)	expr += inst.qW[i]*x[j][i];
			  }
			  model.add(expr <= q[j]); 
			  //~ model.add(expr == q[j]); //Original do Artigo
		  }
		  //1g 
		  for(j = 0; j < n; j++){
		  //1h
			for(i = 0; i < m; i++){
				//Restriction for subgroups
				if(inst.orac[j][i] == 0) model.add(x[j][i] == 0);				
			}
		  }

		IloCplex cplex(model);
		//cplex.setOut(env.getNullStream());
		timer_ms.start();
		cplex.solve();
		cplex_time += timer_ms.total();	  
		  
		float totalTime = timer_global.total();
		//~ output << inst.compressors << "-" << inst.wells << endl;
		//~ output << "Total Time: " << totalTime << endl;
		//~ output << "Time spend to MILP: " << cplex_time << " (" << cplex_time/totalTime*100 << "%)" << endl;		
		//~ output << "FO: " << cplex.getObjValue() << endl;
		output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << cplex_time << "\t" << totalTime << "\t" << cplex.getObjValue() << endl;
	
	   }
	   catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
	   }
	   catch (...) {
		  cerr << "Unknown exception caught" << endl;
	   }
		env.end();	   	
		
		
   }
}
