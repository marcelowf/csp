#include "../include/cg.h"
#include "../include/bprice.h"

using namespace std;

int main(const int argc, char** argv) {
 //~ if (argc != 7) {
	//~ cout << "Error! Choose the correct params" << endl;
	//~ cout << "<Model> <Intance prefix> <initial number> <final number> <timelimit> <coeficentRMP>" << endl;
	//~ cout << "Model: 1 = MILP relaxation; 2 = MILP,  3 = Column Generation; 4 = Branch and Price" << endl;
 //~ }
 
 int modelId = atoi(argv[1]);
 stringstream file;
 double timeLimit = stod(argv[2]);
 file << argv[3];
 double coefRmp = stod(argv[4]);
 
 switch (modelId){
	case 4: csp::bPrice(timeLimit, file.str(), coefRmp);
	break;
	default: cout << "Error, no Model with index " << modelId << " was not found! Try again!"<< endl;
	break;
 }
 
 
return 0;
}
