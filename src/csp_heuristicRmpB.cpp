#include <queue>          
#include <vector>
#include <limits>
#include <math.h>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"

#define RC_LB -1.0e-6
#define RC_UB 1.0e-5
#define NEAR_ONE 0.99999
#define M 1.0e+8

typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using namespace csp;

double CGModel::heuristicRMPB(const Instance& inst, IloEnv& env, const int& nodes, NumNumMatrix& sol){
	struct Column{
		int j;
		int col;		
		double relaxValue; //is the value to sort the priority queue
	};
	struct CompareColumn{
		public:
		bool operator()(const Column& c1,const Column& c2){
			return (c2.relaxValue < c1.relaxValue); //"<" for crescent order; ">" for decrescent
		}
	};
	
	int j, c, i;
	double varValue;		
	//Add columns to priority queue
	priority_queue<Column,vector<Column>, CompareColumn> pQ;	
	for (j = 0; j < inst.compressors; j++){
		sol[j] = IloNumArray(env, inst.wells); //Initialize the vector solution		
		for (c = 0; c < relaxModelValues[j].getSize(); c ++){
			//~ varValue = relaxModelValues[j][c]; //Variable value
			varValue = cjS[j][c]/IloSum(delta[j][c]); //Coeficent divided by the number of supplied clients
			Column col{	j, c, varValue}; 
			pQ.push(col);
		}
	}
	
///From here - Considering a set partitioning, where a column have size = m+n
	for (int tries=0;tries<10;tries++){		
		nextTry: priority_queue<Column,vector<Column>, CompareColumn> pQC;	
		pQC = pQ;
		IloIntArray coveredPoint(env, inst.wells+inst.compressors); // Represents the number of columns that cover each point (the first m numbers are the wells and after the n numbers are the compressors)
		int numCoveredPoints = 0;
		int numCoveredWells = 0;
		double obj = 0;
		
		//Storage the columns in the solution vector<pair> s
		vector< pair<bool, Column> > s;
		int it=0;
		while(numCoveredWells < inst.wells && pQC.size()>1){
			if (it == 0){
				for (int k=0;k<tries;k++) pQC.pop(); //remove the first k columns depending on the tries
			}					
			nextColumn: it++;	
			if(pQC.size() == 0){			
				if(tries < 10){
					tries++;
					goto nextTry;
				}else return -1;
			}			
			Column col = pQC.top();
			pQC.pop();					
			if (it==0){ //in the first iteration just add the column
				s.push_back(make_pair(true,col));				
				obj += cjS[col.j][col.col];
				coveredPoint[inst.wells+col.j]++; //point of compressor
				for (i=0;i<inst.wells;i++){
					if(delta[col.j][col.col][i] > NEAR_ONE){					
						coveredPoint[i]++;						
						numCoveredWells++;
					}
				}
			}else{ //Need analize the column before add
				for (i=0;i<inst.wells;i++){
					if((delta[col.j][col.col][i] > NEAR_ONE && coveredPoint[i] == 1) || coveredPoint[inst.wells+col.j] == 1){						
						goto nextColumn;
					}
				}
				//Only if column does not over cover any point
				s.push_back(make_pair(true,col));
				obj += cjS[col.j][col.col];
				coveredPoint[inst.wells+col.j]++; //point of compressor				
				for (i=0;i<inst.wells;i++){
					if(delta[col.j][col.col][i] > NEAR_ONE){
						coveredPoint[i]++;												
						numCoveredWells++;						
					}
				}
			}
		}	
		//If some compressor is not covered yet
		if (numCoveredPoints < inst.wells+inst.compressors){
			for (j=inst.wells;j<inst.wells+inst.compressors;j++){
				if (coveredPoint[j] < 1){ //Find a compressor not covered
					coveredPoint[j]++;					
					Column col1{j-inst.wells,-1, 0}; //When we use a set with just a compressor, the index of column is '-1' and its cost is zero
					s.push_back(make_pair(true,col1));				
				}
			}		
		}
		if (numCoveredWells == inst.wells){ //Solution is feasible			
			for (j=0;j<s.size();j++){
				//cout << s[j].second.j << " " << s[j].second.col << endl;
				if (s[j].second.col != -1){
					sol[s[j].second.j] = delta[s[j].second.j][s[j].second.col];
				}
			}
			//~ cout << coveredPoint << endl;			
			return obj;
		}
	}
	return -1; //when no solution was found in the tries
}
