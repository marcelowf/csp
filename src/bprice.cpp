//~ #include <queue>          
//~ #include <vector>         
//~ #include <stack>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"


#define RC_LB -1.0e-6
#define RC_UB 1.0e-6
#define NEAR_ONE 0.999999
#define DELIMITER "_"


ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using namespace csp;
			
int cuts = 0;
int nodes = 0;		

void CGModel::getColumnIndices(const IloNumVar& var, int& j, int& c, int& type){
	string varName = var.getName();
	type = atoi(varName.substr(0,1).c_str());
	j = atoi(varName.substr(2, varName.find(DELIMITER)).c_str());
	c = atoi(varName.substr(varName.find(DELIMITER)+1).c_str());	
}

//To find other column of the same compressor when all lines set as 1 were restricted
void CGModel::findOtherSameCompressor(const Instance& inst, IloNum& varValue, const int& compressor, const int& origCol, int& newCol){
	cout << "Coluna " << origCol << endl;
	int type, col, j;
	for(IloExpr::LinearIterator it = cost.getLinearIterator(); it.ok(); ++it){
		double value = cplex.getValue(it.getVar());
		getColumnIndices(it.getVar(), j, col, type);				
		if(j == compressor){			
			if(value > RC_UB && value < NEAR_ONE){								
				if(col != origCol){
					newCol = col;
					varValue = value;					
					break;
				}
			}
		}
	}
}


void CGModel::findMostIntegralVariable(const Instance& inst, IloNum& varValue, int& compressor, int& well, const IloArray<pricing>& subproblem){
	int jMin=-1, jMax=-1;		//To identify the compressor of the most two 'integral' variable
	int j, c;					//Iterators
	int colMin, colMax, col, col1; 	//Indexes and idenficators of the column of 'j' above;
	int typeMin, typeMax, type; //Identificators of type of variable
	IloNum maxV = 0.5, minV = 0.5;
	for(IloExpr::LinearIterator it = cost.getLinearIterator(); it.ok(); ++it){
		double value = cplex.getValue(it.getVar());
		if(value > RC_UB && value < NEAR_ONE){
			if (value >= maxV){ //If closer to 1
				maxV = value;					
				getColumnIndices(it.getVar(), jMax, colMax, typeMax);				
			}
			if (value < minV){ //If closer to 0
				minV = value;					
				getColumnIndices(it.getVar(), jMin, colMin, typeMin);
			}				
		}			
	}
	//Choose the more closer to integrality
	if (1 - minV > maxV){
		varValue = minV;
		compressor = jMin;
		col = colMin;
		type = typeMin;
	}else{
		varValue = maxV;
		compressor = jMax;
		col = colMax;
		type = typeMax;
	}
	col1 = col;
	//~ cout << "Select: lambda[" << compressor << "][" << col1 << "] - Value = " << varValue << endl;
	//~ cout << ">Delta:[";
		//~ for (int ix = 0; ix < inst.wells; ix++){
			//~ cout << delta[compressor][col1][ix] << ", " ;
		//~ }
	//~ cout << "]" << endl;
	
	//Find the well that is supplied by the 'compressor' in the column 'col' thas is not restricted in the pricing yet
	findWell:	
	for (int i = 0; i < inst.wells; i++){	
		//~ cout << delta[compressor][col1][i] << " LB " << subproblem[compressor].x[i].getLB() << endl;			
		if(delta[compressor][col1][i] >= NEAR_ONE && subproblem[compressor].x[i].getLB() < 1){
			well = i;						
			goto fim;
		}
	}	
	//If no well was found
	findOtherSameCompressor(inst, varValue, compressor, col, col1);
	goto findWell;
	fim: return;	
}

void Branch_Price(IloEnv &env, const Instance& inst, CGModel& cgModel, IloArray<pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time, nodeBP& node, IloNum& ub, NumNumMatrix& solution) {
	nodes++;
	if(cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time)){
		int cg_status = cgModel.getStatus(inst.compressors);
		//Infeasible (slack variables)
		if (cg_status == 3){ 
			cuts++;			
			return;
		}
		IloNum lb = cgModel.cplex.getObjValue();		
		cout << "Node: " << nodes << " LB: " << lb << " UB: " << ub << endl;
		//Integral
		if (cg_status == 2){ 			
			cuts++;
			//New best upper bound
			if(lb < ub){
				ub = lb;			
				//~ cout << "New UB! " << ub << endl;	
				///Storage the result
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= NEAR_ONE){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
					}
				}
			}
			return;
		}				
		 //If LB is worst than upper bound
		if (lb > ub){			
			cuts++;
			return;
		}
		///Try to solve the integer program.
		IloArray<IloConversion> conv(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){ 
			conv[j] = IloConversion(env, cgModel.lambda[j], ILOINT);
			cgModel.model.add(conv[j]);			
		}
		if(cgModel.cplex.solve()){
			IloNum obj = cgModel.cplex.getObjValue();	
			if (obj < ub){
				ub = obj;
				///Storage the result
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= NEAR_ONE){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
					}
				}
			}			
			
		}
		for (int j = 0; j < inst.compressors; j++){ //Undo the conversion
			cgModel.model.remove(conv[j]);						
		}
		cgModel.cplex.solve(); //Solve again to get the relaxed variables values
		///
	}else{//Infeasible		
		cuts++;
		return;
	}	 
	///Fractional soltion	
	//Find the lambda variable that is closer to integrality
	IloNum varValue;
	int compressor, well=-1;	
	cgModel.findMostIntegralVariable(inst, varValue, compressor, well, subproblem);	
	IloNumArray b(env, 2); 
	//Define the order of the RHS
	if (varValue < 0.5){
		b[0] = 0;
		b[1] = 1;
	}else{
		b[0] = 1;
		b[1] = 0;
	}		
	for (int i = 0; i < b.getSize(); i++){				
		///Create the node 
		nodeBP nodeB(env, compressor, well, b[i]);
		//Storage all variables indexes of the node in the Objective and set the infeasible columns			
		for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
			int j, c, type;		
			cgModel.getColumnIndices(it.getVar(), j, c, type);
			nodeB.colJ.add(j);
			nodeB.colC.add(c);
			nodeB.colT.add(type);
			if (nodeB.j == j){//For j
				if(b[i] == 0){
					if(cgModel.delta[j][c][ nodeB.i ] > RC_UB){	
						nodeB.colF.add(0);
						//~ cgModel.cost.setLinearCoef(it.getVar(), 100000);
					}else{
						nodeB.colF.add(1);
					}
				}else{//b[i]==1
					if(cgModel.delta[j][c][ nodeB.i ] < NEAR_ONE){	
						nodeB.colF.add(0);
					}else{
						nodeB.colF.add(1);
					}
				}
			}else{ 	//For N\j
				if(b[i] == 0){
					//~ if(cgModel.delta[j][c][ nodeB.i ] < NEAR_ONE){	
						//~ nodeB.colF.add(0);
					//~ }else{
						nodeB.colF.add(1);
					//~ }
				}else{//b[i]==1
					if(cgModel.delta[j][c][ nodeB.i ] > RC_UB){	
						nodeB.colF.add(0);
					}else{
						nodeB.colF.add(1);
					}	
				}				
			}	
		}			

		//Insert the pricing constraints and remove variables of the model
		for (int j = 0 ; j < inst.compressors; j++){
			cgModel.lambda[j].endElements();
			if(b[i] == 0){
				if(j == nodeB.j){
					subproblem[j].x[nodeB.i].setUB(0);
				}else{					
					subproblem[j].x[nodeB.i].setLB(1);
				}
			}else{ //b[i]==1
				if(j == nodeB.j){
					subproblem[j].x[nodeB.i].setLB(1);
				}else{
					subproblem[j].x[nodeB.i].setUB(0);
				}
			}
		}
			
		//Reinsert only FEASIBLE variables of the node		
		for (int k = 0; k < nodeB.colJ.getSize(); k++){
			if(nodeB.colF[k] == 1){
				cgModel.lambda[ nodeB.colJ[k] ].add(
				cgModel.cost(cgModel.cjS[ nodeB.colJ[k] ][ nodeB.colC[k] ]) + //Cost
				cgModel.ClientServiced(cgModel.delta[ nodeB.colJ[k] ][ nodeB.colC[k] ]) + //Constraint B
				cgModel.FacilityAssignment[ nodeB.colJ[k] ](1) );  //Constraint C
				
				//Name
				string s =  to_string(nodeB.colT[k]) + "*" + to_string(nodeB.colJ[k]) + "_" + to_string(nodeB.colC[k]);
				char const* name = s.c_str();
				cgModel.lambda[ nodeB.colJ[k] ][cgModel.lambda[nodeB.colJ[k]].getSize()-1].setName(name);
			}							
		}

		///Branch and price recursive
		Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, nodeB, ub, solution);
		
		//Remove all variables
		for (int j = 0; j < inst.compressors ; j++){
			cgModel.lambda[j].endElements();
			//Remove restrictions of the pricing
			subproblem[j].x[nodeB.i].setBounds(0,1);
		}
		
		//Re-insert the variables of the node
		for (int k = 0; k < nodeB.colJ.getSize(); k++){
			cgModel.lambda[ nodeB.colJ[k] ].add(
			cgModel.cost(cgModel.cjS[ nodeB.colJ[k] ][ nodeB.colC[k] ]) + //Cost
			cgModel.ClientServiced(cgModel.delta[ nodeB.colJ[k] ][ nodeB.colC[k] ]) + //Constraint B
			cgModel.FacilityAssignment[ nodeB.colJ[k] ](1) );  //Constraint C
			
			//Name
			string s =  to_string(nodeB.colT[k]) + "*" + to_string(nodeB.colJ[k]) + "_" + to_string(nodeB.colC[k]);
			char const* name = s.c_str();
			cgModel.lambda[ nodeB.colJ[k] ][cgModel.lambda[nodeB.colJ[k]].getSize()-1].setName(name);
		}		
	}
}

void csp::bPrice(string file, const int n_ini, const int n_fim){
	stringstream out;
	out << 4 << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	
	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj\t" <<  "Nodes\t" << "Cuts\t" << "Solution" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime {0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
				
		IloNum ub = {numeric_limits<double>::max()};	
		cuts = 0;
		nodes = 0;		
		NumNumMatrix solution(env,inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			solution[j] = IloNumArray(env, inst.wells);
		}
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);				

			nodeBP node(env,0,0,0);
			//BEGIN OF RECURSION
			Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, node, ub, solution);
			totalTime += timer_global.total();
			
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << ub << "\t" << nodes << "\t" << cuts << "\t";
			for (int j = 0; j < inst.compressors; j++){
				output << j << "{";
				for (int i = 0; i < inst.wells; i++){
					if (solution[j][i] >= NEAR_ONE) output << i << ",";
				}
				output << "} ";
			}
			output << endl;
			
		}catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
