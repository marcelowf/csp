#pragma once
#include <ilcplex/ilocplex.h>



namespace csp{
typedef IloArray<IloNumArray> NumNumMatrix;

void bPrice(std::string file, const int n_ini, const int n_fim);

struct Column{
int j;
int c;	

Column(IloEnv& env){
	
	};	
};

struct nodeBP{

int j;  //Index of the compressor of lambda selected to branch
int i;	//Index of the well (line of the column) that is supplied by the lambda
int rhs; //Storage the RHS value of the restriction (0 or 1)

//~ IloArray<int> infJ; //Storage the index of infeasible compressor 
//~ IloArray<int> infC; //Storage the index of infeasible column
//~ IloArray<int> infT; //Storage the type of the infeasible olumn (s=slack l=normal)
//~ 
//~ IloArray<int> feasJ; //Storage the index of feasible compressor 
//~ IloArray<int> feasC; //Storage the index of feasible column
//~ IloArray<int> feasT; //Storage the type of the feasible olumn (s=slack l=normal)
//~ 
//~ IloArray<int> genJ; //Storage the index of compressor 
//~ IloArray<int> genC; //Storage the index of column generated
//~ IloArray<int> genT;

IloArray<int> colJ;  	//Storage the index of compressor in the node 
IloArray<int> colC;	//Storage the column
IloArray<int> colT;	//Storage the type of variable (0 - Slack / 1 - Normal)
IloArray<int> colF;	//Set 1 if the column is feasible in the node, 0 otherwise


nodeBP(IloEnv& env, const int& comp, const int& well, const int& rs):
	colJ(env), colC(env), colT(env), colF(env)
	
	{
	
	j = comp;
	i = well;
	rhs = rs;
};


};
	 
}
