#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <ilcplex/ilocplex.h>

#include "util.h"
#include "bprice.h"

namespace csp{


typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

void milp(std::string file, const int n_ini, const int n_fim, const int type, const double& timeLimit);
void milpSos2(std::string file, const int n_ini, const int n_fim, const int type, const double& timeLimit);
void cg(std::string file, const int& n_ini, const int& n_fim, const double& timeLimit); //hot
//~ void cg(const double& timeLimit, std::string file, const int& lfa, const int& minIt, const int& noImpPerc); //iRace CG


struct Instance{
IloInt wells;
IloInt compressors;
IloInt k; //Points in pressure curve of compressor

//Facilities data
IloNumArray cJ; //Cost facilities
IloNumArray dJ ; //Cost for loss energy
IloNumArray qCMin ; //Min Gas-lift rate 
IloNumArray qCMax ; //Max Gas-lift rate 
IloNumArray alpha0 ;
IloNumArray alpha1 ;
IloNumArray alpha2 ;
IloNumArray alpha3 ;
IloNumArray alpha4 ;
NumNumMatrix pointsQ; //Points with gas-rate of compressors 
NumNumMatrix pointsP; //Points pressure
NumNumMatrix pointsH; //Points with operating costs

//Clients data
IloNumArray qW; //gas-lift rate
IloNumArray pW; //pressure

//Client x Facilities data
NumNumMatrix orac; //Oracle (for subsets)
NumNumMatrix cIJ; //Service cost 
NumNumMatrix lIJ;  //Pressure loss in pipeline
NumNumMatrix qMaxIJ; //Max output rate of compressor for servicing wells

Instance(IloEnv& env){};
void readFile(IloEnv& env, std::ifstream& input);
void completeInstance(IloEnv& env, std::fstream& input, int numPoints);
void completeInstanceEC(IloEnv& env, std::ifstream& inComp, std::ifstream& inWell, std::ifstream& inPipe, int numPoints, std::ofstream& output);
void makeInstance(IloEnv& env, std::ifstream& input, int numPoints, std::ofstream& output);
void makeInstanceHolmberg(IloEnv& env, std::ifstream& input, std::ifstream& ref, int numPoints, std::ofstream& output);
void makeInstanceDelmaire(IloEnv& env, std::ifstream& input, std::ifstream& ref, int numPoints, std::ofstream& output);
};

struct pricing{
 IloObjective SPj;
 IloModel model;
 IloCplex cplex;

 //Variables
 IloNumVar q; 
 IloNumVarArray x;
 //~ IloIntVarArray x;
 IloNumVarArray z;
 IloNumVarArray lambdaL;
 IloNumVarArray lambdaR;
 IloSOS2 lambda;
 
 //Params - duals
 IloNum mi;
 IloNumArray pi;
 IloArray<IloRangeArray> BranchConst; //B&P
 
 pricing(IloEnv& env) : SPj(env), model(env), cplex(model){
	model.add(SPj);
 };
 
 void init(IloEnv& env, const Instance& inst, const IloInt& idCompressor); //Create the model according to id off compressor j
 //~ void setDualCoef(const IloInt& j, const IloNum& miD, const IloNumArray& piD, const Instance& inst);
 void setDualCoef(IloEnv& env, const IloInt& j, const IloNum& miD, const IloNumArray& piD, const IloNumArray& psyD, const Instance& inst, const IloNumArray& coef);
 void addConstraint(const IloExpr& expr);
};

struct Solution{
		IloNumArray x; //Solution array
		double q; //best gas rate solution s 
		IloNum c; //best cost solution s
		double rc; //best reduced cost
		double qMin; //min qCmaxIJ
  };

struct CGModel {
  IloObjective cost;
  IloModel model;
  IloCplex cplex;

  //Variable
  NumVarMatrix lambda;

  // constraints
  IloRangeArray ClientServiced;
  IloRangeArray FacilityAssignment;
  IloArray<IloRangeArray> BranchConst; //B&P
  
  // coeficents constraints
  IloArray<NumNumMatrix> delta; // [n] x [#columns] x [m]

  // parameters for calculate coeficent objective
  NumNumMatrix qjS; //Gas-rate do compressor de cada coluna
  NumNumMatrix pjS; //Pressão do compressor em cada coluna
  NumNumMatrix cjS; //Valores objetivos de cada coluna (n * #c)

  NumNumMatrix relaxModelValues; //Storage the value of the variables from the optimal RMP relaxed
  //~ std::vector<std::vector<std::string> > relaxModelNames; //Storage the name of the variables from the optimal RMP relaxed
  
  //Matrix for set partitioning
  NumNumMatrix matrixDelta;
  IloNumArray matrixCost;
  IloIntArray lastNumCol;
  
  CGModel(IloEnv& env) : cost(env), model(env), cplex(model){
	model.add(cost);		
  };
  
  //For two phase method
  Solution getReducedCost(IloEnv& env, const Instance& inst, const int& lfa, const int& j, const IloNum& mi, const IloNumArray& pi, const IloNumArray& psy, const IloIntArray& seeds, const int& cgIt, const int& nodes,
   const IloArray<csp::pricing>& subproblem,
    const int& minIt, const int& noImpPerc);
  Solution randomS(IloEnv& env,const Instance& inst, const int& j, const IloNum& mi, const IloNumArray& pi, const IloNumArray& psy, const int& seed, const IloArray<csp::pricing>& subproblem);
  Solution copySolution(IloEnv& env, const Solution& s);
  
  //Create the model, defining the dimensions off the variables and coeficents
  void init(IloEnv& env, const int& compressors, const int& wells);
  //Create the set of initial columns
  void genInitRMP(IloEnv& env, const Instance& inst, std::unordered_map<std::string,int>& pattern);
  void genInitColumnManual(IloEnv& env, const Instance& inst, std::unordered_map<std::string,int>& pattern);
  void greedyIntialColumns(IloEnv& env, const Instance& inst);
  void greedyIntialColumns(IloEnv& env, const Instance& inst, std::unordered_map<std::string,int>& pattern) ;  
  void addConstraints(IloEnv& env);
  void addColumn(IloEnv env, const IloInt& j, const IloNum& qj, const IloNumArray& coef, const double& qJ, const Instance& inst);
  int getStatus(const IloNum& n);
  
  double getPressure(const Instance& inst, const int& j, const double& qj);
  double costColum(const IloInt& wells, const IloNumArray& delta, const IloInt& compressor, const IloNumArray& cJ, const NumNumMatrix& cIJ, const IloNumArray& dJ, const IloNum& qC, const IloNum& pC);
  //~ bool cg_iter(IloEnv& env, const Instance& inst, IloArray<csp::pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time,
		//~ const nodeBP& node, const double& UB, int& cgIter, int& cpxNodes, int& early_branch, const Timer<std::chrono::milliseconds>& timer_global, 
		//~ const double& timeLimit, const IloIntArray& seeds, const int& nodes,
		//~ const int& lfa, const int& minIt, const int& noImpPerc);	// For iRace CG
  bool cg_iter(IloEnv& env, const Instance& inst, IloArray<csp::pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time,
		const nodeBP& node, const double& UB, int& cgIter, int& cpxNodes, int& early_branch, const Timer<std::chrono::milliseconds>& timer_global, 
		const double& timeLimit, const IloIntArray& seeds, const int& nodes);

  void findCloseIntegralityLevel1(IloEnv& env, const Instance& inst, double& varValue, int& compressor, const IloNumArray& sum_colJ); //For branch level 1
  void findCloseIntegralityLevel2(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well, const NumNumMatrix& sum_colIJ);   //For branch level 2
  
  //~ void findMostIntegralVariable(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well, const IloArray<pricing>& subproblem, const IloIntArray& restricWell);  
  //~ void findOtherSameCompressor(const Instance& inst,double& varValue, const int& compressor, const int& origCol, int& newCol, const IloIntArray& restricWell);
  void findOtherSameCompressor(const Instance& inst,double& varValue, const int& compressor, const int& origCol, int& newCol);
  void getColumnIndices(const IloNumVar& var, int& j, int& c, int& type);
  double setPartitioningMaster(const Instance& inst, IloEnv& env, const int& iter);
  double primalHeuristic(const Instance& inst, IloEnv& env);
  double greedyWSC(const Instance& inst, IloEnv& env);
  double heuristicRMP(const Instance& inst, IloEnv& env, const int& nodes);
  double heuristicRMPB(const Instance& inst, IloEnv& env, const int& nodes, NumNumMatrix& s);
  double shiftProfit(const Instance& inst, const int& i, const int& jA, const int& jB, const double& opCostA, const double& opCostB, const double& gasRateA,const double& gasRateB);
  
  bool canSwap(const Instance& inst, const int& i, const int& i1, const IloIntArray& solutionWell, const IloNumArray& gasRate, const IloNumArray& maxGasRate);
  double swapCost(const Instance& inst, const int& i, const int& i1, const IloIntArray& solutionWell, const IloNumArray& gasRate, const IloNumArray& operCosts);
  void swap(const Instance& inst, const int& i, const int& i1, IloIntArray& solutionWell, IloNumArray& gasRate, IloNumArray& maxGasRate, IloNumArray& operCosts, double& obj);
};



}
