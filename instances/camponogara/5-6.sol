Problem:    cplp
Rows:       114
Columns:    181 (76 integer, 76 binary)
Non-zeros:  588
Status:     INTEGER OPTIMAL
Objective:  cost = 274.557452 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 cost                  274.557
     2 client_facility[1,1]
                                   0                          -0
     3 client_facility[1,2]
                                   0                          -0
     4 client_facility[2,1]
                                   0                          -0
     5 client_facility[2,2]
                                   0                          -0
     6 client_facility[3,1]
                                   0                          -0
     7 client_facility[3,2]
                                   0                          -0
     8 client_facility[3,3]
                                  -1                          -0
     9 client_facility[4,1]
                                  -1                          -0
    10 client_facility[4,2]
                                   0                          -0
    11 client_facility[4,3]
                                   0                          -0
    12 client_facility[4,4]
                                   0                          -0
    13 client_facility[5,1]
                                  -1                          -0
    14 client_facility[5,2]
                                   0                          -0
    15 client_facility[5,3]
                                   0                          -0
    16 client_facility[5,4]
                                   0                          -0
    17 client_facility[5,5]
                                   0                          -0
    18 client_facility[6,1]
                                   0                          -0
    19 client_facility[6,2]
                                   0                          -0
    20 client_facility[6,3]
                                  -1                          -0
    21 client_facility[6,4]
                                   0                          -0
    22 client_facility[6,5]
                                   0                          -0
    23 client_installed[1]
                                   1             1             =
    24 client_installed[2]
                                   1             1             =
    25 client_installed[3]
                                   1             1             =
    26 client_installed[4]
                                   1             1             =
    27 client_installed[5]
                                   1             1             =
    28 client_installed[6]
                                   1             1             =
    29 client_allocation[1,1]
                           -0.096086                          -0
    30 client_allocation[1,2]
                                   0                          -0
    31 client_allocation[2,1]
                                   0                          -0
    32 client_allocation[2,2]
                                   0                          -0
    33 client_allocation[3,1]
                           -0.880347                          -0
    34 client_allocation[3,2]
                                   0                          -0
    35 client_allocation[3,3]
                            -1.38417                          -0
    36 client_allocation[4,1]
                           -0.966939                          -0
    37 client_allocation[4,2]
                                   0                          -0
    38 client_allocation[4,3]
                                   0                          -0
    39 client_allocation[4,4]
                                   0                          -0
    40 client_allocation[5,1]
                           -0.966939                          -0
    41 client_allocation[5,2]
                                   0                          -0
    42 client_allocation[5,3]
                            -1.38417                          -0
    43 client_allocation[5,4]
                                   0                          -0
    44 client_allocation[5,5]
                                   0                          -0
    45 client_allocation[6,1]
                           -0.966938                          -0
    46 client_allocation[6,2]
                                   0                          -0
    47 client_allocation[6,3]
                            -1.38417                          -0
    48 client_allocation[6,4]
                                   0                          -0
    49 client_allocation[6,5]
                                   0                          -0
    50 compressor_flow_demand[1]
                            -1.03306                          -0
    51 compressor_flow_demand[2]
                                   0                          -0
    52 compressor_flow_demand[3]
                           -0.415833                          -0
    53 compressor_flow_demand[4]
                                   0                          -0
    54 compressor_flow_demand[5]
                                   0                          -0
    55 compressor_flow[1]
                                   0            -0             =
    56 compressor_flow[2]
                                   0            -0             =
    57 compressor_flow[3]
                                   0            -0             =
    58 compressor_flow[4]
                                   0            -0             =
    59 compressor_flow[5]
                                   0            -0             =
    60 zeq_y[1]                    0            -0             =
    61 zeq_y[2]                    0            -0             =
    62 zeq_y[3]                    0            -0             =
    63 zeq_y[4]                    0            -0             =
    64 zeq_y[5]                    0            -0             =
    65 zeq_lamb[1,1]
                                   0            -0             =
    66 zeq_lamb[1,2]
                                   0            -0             =
    67 zeq_lamb[1,3]
                                   0            -0             =
    68 zeq_lamb[1,4]
                                   0            -0             =
    69 zeq_lamb[1,5]
                                   0            -0             =
    70 zeq_lamb[1,6]
                                   0            -0             =
    71 zeq_lamb[1,7]
                                   0            -0             =
    72 zeq_lamb[1,8]
                                   0            -0             =
    73 zeq_lamb[1,9]
                                   0            -0             =
    74 zeq_lamb[1,10]
                                   0            -0             =
    75 zeq_lamb[2,1]
                                   0            -0             =
    76 zeq_lamb[2,2]
                                   0            -0             =
    77 zeq_lamb[2,3]
                                   0            -0             =
    78 zeq_lamb[2,4]
                                   0            -0             =
    79 zeq_lamb[2,5]
                                   0            -0             =
    80 zeq_lamb[2,6]
                                   0            -0             =
    81 zeq_lamb[2,7]
                                   0            -0             =
    82 zeq_lamb[2,8]
                                   0            -0             =
    83 zeq_lamb[2,9]
                                   0            -0             =
    84 zeq_lamb[2,10]
                                   0            -0             =
    85 zeq_lamb[3,1]
                                   0            -0             =
    86 zeq_lamb[3,2]
                                   0            -0             =
    87 zeq_lamb[3,3]
                                   0            -0             =
    88 zeq_lamb[3,4]
                                   0            -0             =
    89 zeq_lamb[3,5]
                                   0            -0             =
    90 zeq_lamb[3,6]
                                   0            -0             =
    91 zeq_lamb[3,7]
                                   0            -0             =
    92 zeq_lamb[3,8]
                                   0            -0             =
    93 zeq_lamb[3,9]
                                   0            -0             =
    94 zeq_lamb[3,10]
                                   0            -0             =
    95 zeq_lamb[4,1]
                                   0            -0             =
    96 zeq_lamb[4,2]
                                   0            -0             =
    97 zeq_lamb[4,3]
                                   0            -0             =
    98 zeq_lamb[4,4]
                                   0            -0             =
    99 zeq_lamb[4,5]
                                   0            -0             =
   100 zeq_lamb[4,6]
                                   0            -0             =
   101 zeq_lamb[4,7]
                                   0            -0             =
   102 zeq_lamb[4,8]
                                   0            -0             =
   103 zeq_lamb[4,9]
                                   0            -0             =
   104 zeq_lamb[4,10]
                                   0            -0             =
   105 zeq_lamb[5,1]
                                   0            -0             =
   106 zeq_lamb[5,2]
                                   0            -0             =
   107 zeq_lamb[5,3]
                                   0            -0             =
   108 zeq_lamb[5,4]
                                   0            -0             =
   109 zeq_lamb[5,5]
                                   0            -0             =
   110 zeq_lamb[5,6]
                                   0            -0             =
   111 zeq_lamb[5,7]
                                   0            -0             =
   112 zeq_lamb[5,8]
                                   0            -0             =
   113 zeq_lamb[5,9]
                                   0            -0             =
   114 zeq_lamb[5,10]
                                   0            -0             =

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 y[1]         *              1             0             1
     2 y[2]         *              0             0             1
     3 y[3]         *              1             0             1
     4 y[4]         *              0             0             1
     5 y[5]         *              0             0             1
     6 x[1,1]       *              1             0             1
     7 x[1,2]       *              0             0             1
     8 x[2,1]       *              1             0             1
     9 x[2,2]       *              0             0             1
    10 x[3,1]       *              1             0             1
    11 x[3,2]       *              0             0             1
    12 x[3,3]       *              0             0             1
    13 x[4,1]       *              0             0             1
    14 x[4,2]       *              0             0             1
    15 x[4,3]       *              1             0             1
    16 x[4,4]       *              0             0             1
    17 x[5,1]       *              0             0             1
    18 x[5,2]       *              0             0             1
    19 x[5,3]       *              1             0             1
    20 x[5,4]       *              0             0             1
    21 x[5,5]       *              0             0             1
    22 x[6,1]       *              1             0             1
    23 x[6,2]       *              0             0             1
    24 x[6,3]       *              0             0             1
    25 x[6,4]       *              0             0             1
    26 x[6,5]       *              0             0             1
    27 z[1,1]       *              0             0             1
    28 z[1,2]       *              0             0             1
    29 z[1,3]       *              0             0             1
    30 z[1,4]       *              0             0             1
    31 z[1,5]       *              0             0             1
    32 z[1,6]       *              0             0             1
    33 z[1,7]       *              0             0             1
    34 z[1,8]       *              0             0             1
    35 z[1,9]       *              1             0             1
    36 z[1,10]      *              0             0             1
    37 z[2,1]       *              0             0             1
    38 z[2,2]       *              0             0             1
    39 z[2,3]       *              0             0             1
    40 z[2,4]       *              0             0             1
    41 z[2,5]       *              0             0             1
    42 z[2,6]       *              0             0             1
    43 z[2,7]       *              0             0             1
    44 z[2,8]       *              0             0             1
    45 z[2,9]       *              0             0             1
    46 z[2,10]      *              0             0             1
    47 z[3,1]       *              0             0             1
    48 z[3,2]       *              0             0             1
    49 z[3,3]       *              0             0             1
    50 z[3,4]       *              0             0             1
    51 z[3,5]       *              0             0             1
    52 z[3,6]       *              0             0             1
    53 z[3,7]       *              0             0             1
    54 z[3,8]       *              0             0             1
    55 z[3,9]       *              1             0             1
    56 z[3,10]      *              0             0             1
    57 z[4,1]       *              0             0             1
    58 z[4,2]       *              0             0             1
    59 z[4,3]       *              0             0             1
    60 z[4,4]       *              0             0             1
    61 z[4,5]       *              0             0             1
    62 z[4,6]       *              0             0             1
    63 z[4,7]       *              0             0             1
    64 z[4,8]       *              0             0             1
    65 z[4,9]       *              0             0             1
    66 z[4,10]      *              0             0             1
    67 z[5,1]       *              0             0             1
    68 z[5,2]       *              0             0             1
    69 z[5,3]       *              0             0             1
    70 z[5,4]       *              0             0             1
    71 z[5,5]       *              0             0             1
    72 z[5,6]       *              0             0             1
    73 z[5,7]       *              0             0             1
    74 z[5,8]       *              0             0             1
    75 z[5,9]       *              0             0             1
    76 z[5,10]      *              0             0             1
    77 lambL[1,1]                  0             0
    78 lambL[1,2]                  0             0
    79 lambL[1,3]                  0             0
    80 lambL[1,4]                  0             0
    81 lambL[1,5]                  0             0
    82 lambL[1,6]                  0             0
    83 lambL[1,7]                  0             0
    84 lambL[1,8]                  0             0
    85 lambL[1,9]           0.255765             0
    86 lambL[1,10]                 0             0
    87 lambL[2,1]                  0             0
    88 lambL[2,2]                  0             0
    89 lambL[2,3]                  0             0
    90 lambL[2,4]                  0             0
    91 lambL[2,5]                  0             0
    92 lambL[2,6]                  0             0
    93 lambL[2,7]                  0             0
    94 lambL[2,8]                  0             0
    95 lambL[2,9]                  0             0
    96 lambL[2,10]                 0             0
    97 lambL[3,1]                  0             0
    98 lambL[3,2]                  0             0
    99 lambL[3,3]                  0             0
   100 lambL[3,4]                  0             0
   101 lambL[3,5]                  0             0
   102 lambL[3,6]                  0             0
   103 lambL[3,7]                  0             0
   104 lambL[3,8]                  0             0
   105 lambL[3,9]           0.977381             0
   106 lambL[3,10]                 0             0
   107 lambL[4,1]                  0             0
   108 lambL[4,2]                  0             0
   109 lambL[4,3]                  0             0
   110 lambL[4,4]                  0             0
   111 lambL[4,5]                  0             0
   112 lambL[4,6]                  0             0
   113 lambL[4,7]                  0             0
   114 lambL[4,8]                  0             0
   115 lambL[4,9]                  0             0
   116 lambL[4,10]                 0             0
   117 lambL[5,1]                  0             0
   118 lambL[5,2]                  0             0
   119 lambL[5,3]                  0             0
   120 lambL[5,4]                  0             0
   121 lambL[5,5]                  0             0
   122 lambL[5,6]                  0             0
   123 lambL[5,7]                  0             0
   124 lambL[5,8]                  0             0
   125 lambL[5,9]                  0             0
   126 lambL[5,10]                 0             0
   127 lambR[1,1]                  0             0
   128 lambR[1,2]                  0             0
   129 lambR[1,3]                  0             0
   130 lambR[1,4]                  0             0
   131 lambR[1,5]                  0             0
   132 lambR[1,6]                  0             0
   133 lambR[1,7]                  0             0
   134 lambR[1,8]                  0             0
   135 lambR[1,9]           0.744235             0
   136 lambR[1,10]                 0             0
   137 lambR[2,1]                  0             0
   138 lambR[2,2]                  0             0
   139 lambR[2,3]                  0             0
   140 lambR[2,4]                  0             0
   141 lambR[2,5]                  0             0
   142 lambR[2,6]                  0             0
   143 lambR[2,7]                  0             0
   144 lambR[2,8]                  0             0
   145 lambR[2,9]                  0             0
   146 lambR[2,10]                 0             0
   147 lambR[3,1]                  0             0
   148 lambR[3,2]                  0             0
   149 lambR[3,3]                  0             0
   150 lambR[3,4]                  0             0
   151 lambR[3,5]                  0             0
   152 lambR[3,6]                  0             0
   153 lambR[3,7]                  0             0
   154 lambR[3,8]                  0             0
   155 lambR[3,9]          0.0226186             0
   156 lambR[3,10]                 0             0
   157 lambR[4,1]                  0             0
   158 lambR[4,2]                  0             0
   159 lambR[4,3]                  0             0
   160 lambR[4,4]                  0             0
   161 lambR[4,5]                  0             0
   162 lambR[4,6]                  0             0
   163 lambR[4,7]                  0             0
   164 lambR[4,8]                  0             0
   165 lambR[4,9]                  0             0
   166 lambR[4,10]                 0             0
   167 lambR[5,1]                  0             0
   168 lambR[5,2]                  0             0
   169 lambR[5,3]                  0             0
   170 lambR[5,4]                  0             0
   171 lambR[5,5]                  0             0
   172 lambR[5,6]                  0             0
   173 lambR[5,7]                  0             0
   174 lambR[5,8]                  0             0
   175 lambR[5,9]                  0             0
   176 lambR[5,10]                 0             0
   177 qc[1]                 12.0331             0
   178 qc[2]                       0             0
   179 qc[3]                 9.41583             0
   180 qc[4]                       0             0
   181 qc[5]                       0             0

Integer feasibility conditions:

INT.PE: max.abs.err. = 3.62e-14 on row 1
        max.rel.err. = 1.31e-16 on row 1
        High quality

INT.PB: max.abs.err. = 1.04e-17 on row 93
        max.rel.err. = 1.04e-17 on row 93
        High quality

End of output
