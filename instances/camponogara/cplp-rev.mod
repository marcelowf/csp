#------------------------------------------------------
# cp.mod
#------------------------------------------------------

param n > 0 integer;  # number of facilities (compressors)
param m > 0 integer;  # number of clients (wells)
param kj > 0 integer; # number of points

set N  := 1..n;
set M  := 1..m;
set K  := 1..kj;
set K0 := 0..kj;

param a {N} >=0;
param d {N} >=0;
param qjmax {N} >=0;

set C within (M cross N);
param c {(i,j) in C} >= 0;

param l {(i,j) in C} >=0;
param qijmax {(i,j) in C} >=0;

param qw {M} >= 0;
param pw {M} >= 0;

param qcpt { j in N, k in K0};
param pcpt { j in N, k in K0};
param fcpt { j in N, k in K0};


#--------------------------------------------------------------
var y {N} integer >=0, <=1;
var x {(i,j) in C} integer >=0, <=1;
var z { j in N, k in K} integer >=0, <=1;
var lambL { j in N, k in K} >= 0;
var lambR { j in N, k in K} >= 0;

var qc {N} >=0;


#--------------------------------------------------------------
minimize cost: sum{j in N} a[j]*y[j] 
               + sum{(i,j) in C} c[i,j]*x[i,j] 
               + sum{j in N} d[j]* sum{k in K}( fcpt[j,k-1]*lambL[j,k] + fcpt[j,k]*lambR[j,k]); 

#--------------------------------------------------------------
subject to client_facility{(i,j) in C}:
    x[i,j] <= y[j];

subject to client_installed{i in M}:
    sum{(i,j) in C} x[i,j] == 1;

subject to client_allocation{(i,j) in C}:
    qc[j] <= qijmax[i,j]*x[i,j] + qjmax[j]*(y[j] - x[i,j]);

subject to compressor_flow_demand{j in N}:
    sum{(i,j) in C} qw[i] * x[i,j] <= qc[j];

subject to compressor_flow{j in N}:
     qc[j] = sum{k in K} (qcpt[j,k-1]*lambL[j,k] + qcpt[j,k]*lambR[j,k]);

subject to zeq_y{j in N}:
    sum{k in K} z[j,k] = y[j];

subject to zeq_lamb{j in N, k in K}:
    lambL[j,k] + lambR[j,k] = z[j,k];

