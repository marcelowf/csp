#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

using namespace std;
int clients;
int facilities;
int k; //Points in pressure curve of compressor

//Facilities data
vector<double> cJ; //Cost facilities
vector<double> dJ ; //Cost for loss energy
vector<double> qCMin ; //Min Gas-lift rate 
vector<double> qCMax ; //Max Gas-lift rate 
vector<double> alpha0 ;
vector<double> alpha1 ;
vector<double> alpha2 ;
vector<double> alpha3 ;
vector<double> alpha4 ;
vector<vector<double> > pointsQ; //Points with gas-rate of compressors 
vector<vector<double> > pointsH; //Points with operating costs

//Clients data
vector<double> qW; //gas-lift rate
vector<double> pW; //pressure

//Both data
vector<vector<double> >orac; //Oracle (for subsets)
vector<vector<double> >cIJ ; //Service cost 
vector<vector<double> >lIJ; //Pressure loss in pipeline
vector<vector<double> >qMaxIJ; //Max output rate of compressor for servicing wells

//Subsets
//int Ni[clients]; //Compressors that can service well i
//int Mj[facilities]; //Wells that can be serviced by compressor j

void loadInstance(ifstream &input, int &clients, int &facilities, int &k, 
		vector<double> &cJ, vector<double> &dJ, vector<double> &qCMin, 
		vector<double> &qCMax, vector<double> &alpha0, vector<double> &alpha1,
		vector<double> &alpha2, vector<double> &alpha3, vector<double> &alpha4,
		vector<vector<double> > &pointsQ, vector<vector<double> > &pointsH,
		vector<double> &qW, vector<double> &pW,
		vector<vector<double> > &cIJ, vector<vector<double> > &lIJ, vector<vector<double> > &qMaxIJ, vector<vector<double> > &orac){
			
	string s;
	double temp, c, d, x;
	//Parameters
	input >> facilities >> clients >> k;
	
	cJ.reserve(facilities);
	dJ.reserve(facilities);
	qCMin.reserve(facilities);
	qCMax.reserve(facilities);
	alpha0.reserve(facilities);
	alpha1.reserve(facilities);
	alpha2.reserve(facilities);
	alpha3.reserve(facilities);
	alpha4.reserve(facilities);
	
	pointsQ.reserve(facilities);
	pointsH.reserve(facilities);
	for (int i = 0; i < facilities; i++){
		pointsQ[i].reserve(k);
		pointsH[i].reserve(k);
	}
	qW.reserve(clients);
	pW.reserve(clients);
	
	cIJ.reserve(clients);
	lIJ.reserve(clients);
	qMaxIJ.reserve(clients);
	orac.reserve(clients);
	for (int i = 0; i < clients; i++){
		cIJ[i].reserve(facilities);
		lIJ[i].reserve(facilities);
		qMaxIJ[i].reserve(facilities);
		orac[i].reserve(facilities);
	}
	
	/*****Scan file*****/
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}	
	//Compressor data	
	for (int i = 0; i < facilities; i++){
		input >> temp >> cJ[i] >> dJ[i] >> qCMin[i] >> qCMax[i] >> alpha0[i] >> alpha1[i] >> alpha2[i] >> alpha3[i] >> alpha4[i];
	}
	
	
	//Well data
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}
	for (int i = 0; i < clients; i++){
		input >> temp >> qW[i] >> pW[i]; //percorre até pw
		getline(input, s); //pega o Ni
		stringstream ss(s); //Define a string como stream
		while(ss >> c){ //Pega os doubles
			orac[i][c-1] = 1;						
		}
	}
	//Pipeline
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}	
	while(getline(input, s)){
		if(s == "Service cost")break;
		stringstream(s) >> temp >> x >> c;
		lIJ[temp-1][x-1] = c;
	}
	
	//Service cost
	getline(input, s);
	while(getline(input, s)){
		if(s == "Max output gas-rate")break;
		stringstream(s) >> temp >> x >> c;
		cIJ[temp-1][x-1] = c;
		//cout << temp << ":" << x << "=" << cIJ[temp-1][x-1] << endl;
	}
	
	//Max output gas-rate
	getline(input, s);
	while(getline(input, s)){
		if(s == "Points")break;
		stringstream(s) >> temp >> x >> c;
		qMaxIJ[temp-1][x-1] = c;
		//cout << temp << ":" << x << "=" << qMaxIJ[temp-1][x-1] << endl;
	}
	
	//Points
	getline(input, s);
	while(getline(input, s)){
		double p;
		stringstream(s) >> temp >> x >> c >> p;
		pointsQ[temp-1][x] = c;
		pointsH[temp-1][x] = p;
		//cout << "Facility: " << temp << " Point: " << x << " Q: " << pointsQ[temp-1][x] << " P: " << pointsH[temp-1][x] << endl;
	}
	//Print matrix orac
	for (int i = 0; i < clients; i++){
		for (int j = 0; j < facilities; j++){
			cout << "Client: " << i << " Facility: " << j <<": " << orac[i][j] << endl;
		}
	}
		//cout << "Tamanho de cJ (Facilities): " << cIJ.size() << endl;
}

int main(int argc, char *argv[]) {
	ifstream inFile;
	inFile.open("cspData.txt");
	
	if (inFile.fail()) {
      cerr << "Unable to open instance"<< endl;
      //exit(1);
	}
	
	loadInstance(inFile, clients, facilities, k, 
		cJ, dJ, qCMin, 
		qCMax, alpha0, alpha1, alpha2, alpha3, alpha4,
		pointsQ, pointsH,
		qW, pW,
		cIJ, lIJ, qMaxIJ, orac);
	
	for (int i = 0; i < facilities; i++){
		cout << "Custo compressor " << i << " " << cJ[i] << endl;
	}
	for (int i = 0; i < clients; i++){
		cout << "Pressao poços " << i << " " << pW[i] << endl;
	}
	
	 return 0;
}
